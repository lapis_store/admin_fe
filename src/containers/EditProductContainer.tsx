import * as React from 'react';
import api from '../api/api';
import storage from '../storage';

export interface IEditProductContainerProps {
    id?: string;
}

export default function EditProductContainer(props: React.PropsWithChildren<IEditProductContainerProps>) {
    const { dispatchStore, actions } = storage.editProduct.useEditProduct();

    const loadProduct = React.useCallback(
        async (productId?: string) => {
            const productResponse = await api.product.find(productId);
            dispatchStore(actions.response(productResponse));
        },
        [dispatchStore, actions],
    );

    React.useEffect(() => {
        if (!props.id) return undefined;

        // create new
        if (props.id === 'new') return undefined;

        // update
        loadProduct(props.id);
    }, [props.id, loadProduct]);

    return <>{props.children}</>;
}
