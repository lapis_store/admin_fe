import * as React from 'react';
import api from '../api/api';
import apiFindProductDetail from '../api/apiFindProductDetail';
// import IProductDetailResponse from '../share_types/response/admin/IProductDetailResponse';
// import IProductResponse from '../share_types/response/admin/IProductResponse';
import storage from '../storage';

export interface IEditProductDetailContainerProps {
    id?: string;
}

function EditProductDetailContainer(props: React.PropsWithChildren<IEditProductDetailContainerProps>) {
    const { dispatchStore, actions } = storage.productDetail.useProductDetail();

    const load = React.useCallback(
        async (productId?: string) => {
            if (!productId) return;

            const [productResponse, productDetailResponse] = await Promise.all([
                api.product.find(productId),
                apiFindProductDetail(productId),
            ]);

            dispatchStore(actions.productResponse(productResponse));
            dispatchStore(actions.response(productDetailResponse));
        },
        [dispatchStore, actions],
    );

    React.useEffect(() => {
        load(props.id);
    }, [props.id, load]);

    return <>{props.children}</>;
}

export default EditProductDetailContainer;
