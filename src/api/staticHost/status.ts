import ENV_SHARE from '../../share/core/ENV_SHARE';
import axiosWithAuth from '../middleware/axiosWithAuth';

export interface IStatusResponse {
    lastUpdate: number;
    processing: number;
}

const url = [ENV_SHARE.STATIC_HOST, 'admin', 'images', 'status'].join('/');

const status = async (): Promise<IStatusResponse | undefined> => {
    try {
        const res = await axiosWithAuth.get(url);
        if (res.status !== 200) {
            return undefined;
        }

        const resData: IStatusResponse = res.data;
        return resData;
    } catch {
        return undefined;
    }
};

export default status;
