import axios from 'axios';
import ENV from '../../core/ENV';

export default async function list(): Promise<string[] | undefined> {
    const res = await axios.get(`${ENV.STATIC_HOST}/admin/images/list`);

    if (res.status !== 200) {
        return undefined;
    }

    return res.data;
}
