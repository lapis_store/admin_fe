import ENV_SHARE from '../../share/core/ENV_SHARE';
import axiosWithAuth from '../middleware/axiosWithAuth';

export interface IRemoveResponse {
    status: 'successfully' | 'failure';
}

const url = [ENV_SHARE.STATIC_HOST, 'admin', 'images', 'remove'].join('/');

const remove = async (v: string): Promise<IRemoveResponse | undefined> => {
    try {
        const res = await axiosWithAuth.delete(url, {
            params: {
                imageName: v,
            },
        });

        if (res.status !== 201) {
            return undefined;
        }

        const resData: IRemoveResponse = res.data;
        return resData;
    } catch {
        return undefined;
    }
};

export default remove;
