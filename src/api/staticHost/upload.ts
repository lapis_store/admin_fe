import ENV from '../../core/ENV';
import axiosWithAuth from '../middleware/axiosWithAuth';

export interface IUploadImageResponse {
    status: 'successfully' | 'failed';
    message: string;
}

const url = [ENV.STATIC_HOST, 'admin', 'upload'].join('/');

export default async function upload(formData: FormData): Promise<IUploadImageResponse> {
    try {
        const res = await axiosWithAuth({
            url,
            method: 'POST',
            data: formData,
        });

        console.log(res.data);

        if (res.status !== 201) {
            return {
                status: 'failed',
                message: String(res.data),
            };
        }

        return {
            status: 'successfully',
            message: '',
        };
    } catch (e) {
        console.log(e);
        return {
            status: 'failed',
            message: 'Có một lỗi không xác định trong quá trình upload',
        };
    }
}
