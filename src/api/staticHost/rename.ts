import ENV_SHARE from '../../share/core/ENV_SHARE';
import axiosWithAuth from '../middleware/axiosWithAuth';

export interface IStatusResponse {
    lastUpdate: number;
    processing: number;
}

const rename = async (imageFileName: string, newImageName: string): Promise<any> => {
    const url = [ENV_SHARE.STATIC_HOST, 'admin', 'images', 'rename', imageFileName].join('/');
    try {
        const res = await axiosWithAuth.patch(url, {
            newImageName,
        });

        if (res.status !== 200) {
            return undefined;
        }

        const resData = res.data;
        return resData;
    } catch {
        return undefined;
    }
};

export default rename;
