import list from './list';
import remove from './remove';
import rename from './rename';
import status from './status';
import upload from './upload';

const staticHost = {
    rename,
    remove,
    status,
    list,
    upload,
};

export default staticHost;
