import ENV from '../../core/ENV';
import IProductFormData from '../../share_types/form_data/admin/IProductFormData';
import IProductResponse from '../../share_types/response/admin/IProductResponse';
import axiosWithAuth from '../middleware/axiosWithAuth';

const url = [ENV.API_HOST, 'admin', 'product', 'create'].join('/');

const create = async (formData: IProductFormData): Promise<IProductResponse | undefined> => {
    try {
        const res = await axiosWithAuth.post(url, {
            ...formData,
        });

        if (res.status !== 201) return undefined;
        const productResponse: IProductResponse = res.data;
        return productResponse;
    } catch {
        return undefined;
    }
};

export default create;
