import ENV from '../../core/ENV';
import IRemoveResponse from '../../share_types/response/admin/IRemoveResponse';
import axiosWithAuth from '../middleware/axiosWithAuth';

export default async function remove(productId?: string): Promise<IRemoveResponse | undefined> {
    console.log(productId);
    if (!productId) return undefined;

    try {
        const res = await axiosWithAuth.delete(`${ENV.API_HOST}/admin/product/remove/${productId}`);
        if (res.status !== 200) return undefined;

        const resData: IRemoveResponse = res.data;
        return resData;
    } catch {
        return undefined;
    }
}
