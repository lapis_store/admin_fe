import checkSlug from './checkSlug';
import create from './create';
import find from './find';
import imageName from './imageName';
import list from './list';
import remove from './remove';
import update from './update';
const product = {
    imageName,
    checkSlug,
    find,
    update,
    create,
    list,
    remove,
};

export default product;
