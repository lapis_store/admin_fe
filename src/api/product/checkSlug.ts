import ENV from '../../core/ENV';
import ICheckSlugResponse from '../../share_types/response/admin/ICheckSlugResponse';
import axiosWithAuth from '../middleware/axiosWithAuth';

export default async function checkSlug(slug: string, id?: string): Promise<ICheckSlugResponse | undefined> {
    const productId: string | undefined = !id ? undefined : String(id);

    try {
        const res = await axiosWithAuth.get(`${ENV.API_HOST}/admin/product/check-slug`, {
            params: {
                slug,
                id: productId,
            },
        });

        if (res.status !== 200) {
            return undefined;
        }

        const checkSlugResponse: ICheckSlugResponse = res.data;
        return checkSlugResponse;
    } catch (e) {
        return undefined;
    }
}
