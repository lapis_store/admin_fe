import ENV from '../../core/ENV';
import axiosWithAuth from '../middleware/axiosWithAuth';

async function imageName(productId: string): Promise<string | undefined> {
    const url = [ENV.API_HOST, 'admin', 'product', 'image-name', productId].join('/');

    try {
        const res = await axiosWithAuth.get(url);
        if (res.status !== 200) {
            return undefined;
        }

        return res.data;
    } catch (e) {
        return undefined;
    }
}

export default imageName;
