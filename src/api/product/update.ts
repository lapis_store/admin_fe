import ENV from '../../core/ENV';
import IProductFormData from '../../share_types/form_data/admin/IProductFormData';
import IProductResponse from '../../share_types/response/admin/IProductResponse';
import axiosWithAuth from '../middleware/axiosWithAuth';

export default async function update(
    productId: string,
    formData: IProductFormData,
): Promise<IProductResponse | undefined> {
    const url = `${ENV.API_HOST}/admin/product/update/${productId}`;
    try {
        const res = await axiosWithAuth.patch(url, {
            ...formData,
        });

        if (res.status !== 200) return undefined;

        return res.data;
    } catch (e) {
        return undefined;
    }
}
