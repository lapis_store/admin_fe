import ENV from '../../core/ENV';
import IProductResponse from '../../share_types/response/admin/IProductResponse';
import axiosWithAuth from '../middleware/axiosWithAuth';

async function find(productId?: string): Promise<IProductResponse | undefined> {
    if (!productId) return;

    try {
        const res = await axiosWithAuth.get(`${ENV.API_HOST}/admin/product/find/${productId}`);
        if (res.status !== 200) {
            return undefined;
        }

        const productResponse: IProductResponse = res.data;
        return productResponse;
    } catch (e) {
        return undefined;
    }
}

export default find;
