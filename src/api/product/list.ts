import ENV from '../../core/ENV';
import IProductResponse from '../../share_types/response/admin/IProductResponse';
import axiosWithAuth from '../middleware/axiosWithAuth';

const url: string = [ENV.API_HOST, 'admin', 'product', 'list'].join('/');

async function list(categoryId?: string): Promise<IProductResponse[] | undefined> {
    try {
        const res = await axiosWithAuth.get(url, {
            params: {
                categoryId,
            },
        });

        if (res.status !== 200) return undefined;

        if (!Array.isArray(res.data)) return undefined;

        const productsResponse: IProductResponse[] = res.data;
        return productsResponse;
    } catch {
        return undefined;
    }
}

export default list;
