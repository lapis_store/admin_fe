import ENV_SHARE from '../../share/core/ENV_SHARE';
import ICategoryResponse from '../../share_types/response/admin/category/ICategoryResponse';
import axiosWithAuth from '../middleware/axiosWithAuth';

const url = [ENV_SHARE.API_HOST, 'admin', 'category', 'list'].join('/');

const list = async (): Promise<ICategoryResponse[] | undefined> => {
    try {
        const res = await axiosWithAuth.get(url);

        if (res.status !== 200) {
            return undefined;
        }

        const resData: ICategoryResponse[] = res.data;
        return resData;
    } catch {
        return undefined;
    }
};

export default list;
