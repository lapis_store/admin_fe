import ENV_SHARE from '../../share/core/ENV_SHARE';
import ICategoryFormData from '../../share_types/form_data/admin/ICategoryFormData';
import ICategoryResponse from '../../share_types/response/admin/category/ICategoryResponse';
import axiosWithAuth from '../middleware/axiosWithAuth';

const url = [ENV_SHARE.API_HOST, 'admin', 'category', 'create'].join('/');
const create = async (formData: ICategoryFormData) => {
    try {
        const res = await axiosWithAuth.post(url, formData);
        if (res.status !== 201) return undefined;

        const resData: ICategoryResponse = res.data;
        return resData;
    } catch {
        return undefined;
    }
};

export default create;
