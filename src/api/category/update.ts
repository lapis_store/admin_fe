import ENV_SHARE from '../../share/core/ENV_SHARE';
import ICategoryFormData from '../../share_types/form_data/admin/ICategoryFormData';
import ICategoryResponse from '../../share_types/response/admin/category/ICategoryResponse';
import axiosWithAuth from '../middleware/axiosWithAuth';

const update = async (categoryId: string, formData: ICategoryFormData) => {
    const url = [ENV_SHARE.API_HOST, 'admin', 'category', 'update', categoryId].join('/');
    try {
        const res = await axiosWithAuth.patch(url, formData);
        if (res.status !== 200) return undefined;

        const resData: ICategoryResponse = res.data;
        return resData;
    } catch {
        return undefined;
    }
};

export default update;
