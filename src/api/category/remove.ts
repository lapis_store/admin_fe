import ENV_SHARE from '../../share/core/ENV_SHARE';
import ICategoryResponse from '../../share_types/response/admin/category/ICategoryResponse';
import axiosWithAuth from '../middleware/axiosWithAuth';

const remove = async (categoryId: string, removeType: 'one' | 'all') => {
    const url = [
        ENV_SHARE.API_HOST,
        'admin',
        'category',
        removeType === 'one' ? 'remove' : 'removeAll',
        categoryId,
    ].join('/');
    try {
        const res = await axiosWithAuth.delete(url);
        if (res.status !== 200) return undefined;

        const resData: ICategoryResponse = res.data;
        return resData;
    } catch {
        return undefined;
    }
};

export default remove;
