import create from './create';
import list from './list';
import remove from './remove';
import update from './update';
const category = {
    remove,
    update,
    create,
    list,
};

export default category;
