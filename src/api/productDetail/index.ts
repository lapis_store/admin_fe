import create from './create';
import update from './update';

const productDetail = {
    update,
    create,
};

export default productDetail;
