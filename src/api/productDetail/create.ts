import ENV from '../../core/ENV';
import IProductDetailFormData from '../../share_types/form_data/admin/IProductDetailFormData';
import IProductDetailResponse from '../../share_types/response/admin/IProductDetailResponse';
import axiosWithAuth from '../middleware/axiosWithAuth';

export default async function create(formData: IProductDetailFormData): Promise<IProductDetailResponse | undefined> {
    const url: string = `${ENV.API_HOST}/admin/product-detail/create`;
    try {
        const res = await axiosWithAuth.post(url, {
            ...formData,
        });

        if (res.status === 201) return res.data;
        return undefined;
    } catch (e) {
        return undefined;
    }
}
