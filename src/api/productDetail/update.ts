import ENV from '../../core/ENV';
import IProductDetailFormData from '../../share_types/form_data/admin/IProductDetailFormData';
import IProductDetailResponse from '../../share_types/response/admin/IProductDetailResponse';
import axiosWithAuth from '../middleware/axiosWithAuth';

const url: string = `${ENV.API_HOST}/admin/product-detail/update`;

export default async function update(formData: IProductDetailFormData): Promise<IProductDetailResponse | undefined> {
    try {
        const res = await axiosWithAuth.patch(url, {
            ...formData,
        });

        if (res.status !== 200) return undefined;
        return res.data;
    } catch (e) {
        return undefined;
    }
}
