import category from './category';
import home from './home';
import login from './login';
import order from './order';
import product from './product';
import productDetail from './productDetail';
import staticHost from './staticHost';

const api = {
    home,
    productDetail,
    login,
    order,
    product,
    staticHost,
    category,
};

export default api;
