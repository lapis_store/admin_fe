import ENV from '../../core/ENV';
import IHomeFormData from '../../share_types/form_data/admin/IHomeFormData';
import IHomeResponse from '../../share_types/response/home/IHomeResponse';
import axiosWithAuth from '../middleware/axiosWithAuth';

const url = [ENV.API_HOST, 'admin', 'home', 'create'].join('/');

const create = async (formData: IHomeFormData): Promise<IHomeResponse | undefined> => {
    try {
        const res = await axiosWithAuth.post(url, {
            ...formData,
        });

        if (res.status !== 201) return undefined;
        return res.data;
    } catch {
        return undefined;
    }
};

export default create;
