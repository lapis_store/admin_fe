import create from './create';
import getData from './getData';

const home = {
    getData,
    create,
};

export default home;
