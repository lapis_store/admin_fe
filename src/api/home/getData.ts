import ENV from '../../core/ENV';
import IHomeResponse from '../../share_types/response/home/IHomeResponse';
import axiosWithAuth from '../middleware/axiosWithAuth';

const url = [ENV.API_HOST, 'static', 'data', 'home.json'].join('/');

const getData = async (): Promise<IHomeResponse | undefined> => {
    try {
        const res = await axiosWithAuth.get(url);
        if (res.status !== 200) return undefined;
        return res.data;
    } catch {
        return undefined;
    }
};

export default getData;
