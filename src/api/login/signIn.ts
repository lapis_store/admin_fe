import LapisLocalStorage from '../../core/LapisLocalStorage';
import ENV from '../../core/ENV';
import IAdminLoginResponse from '../../share_types/response/login/IAdminLoginResponse';
import axiosWithAuth from '../middleware/axiosWithAuth';

const url = [ENV.API_HOST, 'login', 'admin-login', 'sign-in'].join('/');

const signIn = async (userName: string, password: string): Promise<Pick<IAdminLoginResponse, 'status'> | undefined> => {
    try {
        const res = await axiosWithAuth.post(url, {
            userName,
            password,
        });

        if (res.status !== 200) return undefined;

        const loginRes: IAdminLoginResponse = res.data;

        const { status, accessToken } = loginRes;

        if (status === undefined) return undefined;

        // save token if exists
        if (typeof accessToken === 'string') {
            LapisLocalStorage.instance.accessToken = accessToken;
        }

        return {
            status,
        };
    } catch {
        return undefined;
    }
};

export default signIn;
