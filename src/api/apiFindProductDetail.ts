import axios from 'axios';
import ENV from '../core/ENV';
import IProductDetailResponse from '../share_types/response/admin/IProductDetailResponse';

export default async function apiFindProductDetail(productId: string): Promise<IProductDetailResponse> {
    const url: string = `${ENV.API_HOST}/admin/product-detail/find/${productId}`;

    try {
        const res = await axios({
            url,
            method: 'GET',
        });

        // not found
        if (res.status !== 200) {
            return {} as any;
        }

        return res.data;
    } catch (e) {
        return {} as any;
    }
}
