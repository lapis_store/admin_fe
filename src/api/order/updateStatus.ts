import ENV from '../../core/ENV';
import EOrderStatus from '../../share_types/database/EOrderStatus';
import IOrderResponse from '../../share_types/response/admin/order/TListOrderResponse';
import axiosWithAuth from '../middleware/axiosWithAuth';

const updateStatus = async (orderId: string, orderStatus: EOrderStatus): Promise<IOrderResponse | undefined> => {
    const url = [ENV.API_HOST, 'admin', 'order', 'update-status', orderId].join('/');
    try {
        const res = await axiosWithAuth.patch(url, {
            orderStatus,
        });

        if (res.status !== 200) {
            return undefined;
        }

        return res.data;
    } catch {
        return undefined;
    }
};

export default updateStatus;
