import ENV from '../../core/ENV';
import axiosWithAuth from '../middleware/axiosWithAuth';

export interface IBaseStatisticsResponse {
    totalEarned: number;
    totalOrdersPlaced: number;
    averageEarnedInDays: number;
    averageOrdersPlacedInDays: number;
}

const url = [ENV.API_HOST, 'admin', 'order', 'base-statistics'].join('/');

const baseStatistics = async (): Promise<IBaseStatisticsResponse | undefined> => {
    try {
        const res = await axiosWithAuth.get(url);
        if (res.status !== 200) return undefined;

        return res.data;
    } catch {
        return undefined;
    }
};

export default baseStatistics;
