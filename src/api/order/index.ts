import baseStatistics from './baseStatistics';
import find from './find';
import list from './list';
import updateStatus from './updateStatus';
const order = {
    find,
    baseStatistics,
    updateStatus,
    list,
};

export default order;
