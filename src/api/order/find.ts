import ENV_SHARE from '../../share/core/ENV_SHARE';
import IOrderResponse from '../../share_types/response/admin/order/TListOrderResponse';
import axiosWithAuth from '../middleware/axiosWithAuth';

const find = async (id: string): Promise<IOrderResponse | undefined> => {
    const url = [ENV_SHARE.API_HOST, 'admin', 'order', 'find', id].join('/');

    try {
        const res = await axiosWithAuth.get(url);

        if (res.status !== 200) return undefined;

        const resData: IOrderResponse = res.data;
        return resData;
    } catch {
        return undefined;
    }
};

export default find;
