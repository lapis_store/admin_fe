import ENV_SHARE from '../../share/core/ENV_SHARE';
import IOrderResponse from '../../share_types/response/admin/order/TListOrderResponse';
import axiosWithAuth from '../middleware/axiosWithAuth';

const list = async (options?: { startAt?: Date; endAt?: Date }): Promise<IOrderResponse[] | undefined> => {
    const url = `${ENV_SHARE.API_HOST}/admin/order/list`;

    try {
        const res = await axiosWithAuth.get(url, {
            params: {
                ...options,
            },
        });

        if (res.status !== 200) return undefined;

        const resData: IOrderResponse[] = res.data;
        return resData;
    } catch {
        return undefined;
    }
};

export default list;
