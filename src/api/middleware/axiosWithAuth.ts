import axios, { AxiosError } from 'axios';
import LapisLocalStorage from '../../core/LapisLocalStorage';

axios.interceptors.request.use(
    (config) => {
        return {
            ...config,
            headers: {
                ...config.headers,
                authorization: LapisLocalStorage.instance.accessToken,
            },
        };
    },
    (err) => {
        return Promise.reject(err);
    },
);

axios.interceptors.response.use(
    (res) => {
        return res;
    },
    (error: AxiosError) => {
        const status = error.response?.status;

        if ((status === 401 && !LapisLocalStorage.instance.accessToken) || status === 403) {
            LapisLocalStorage.instance.accessToken = '';
            if (window.location.href.endsWith('/login')) {
                return;
            }
            window.location.href = '/login';
        }

        return error;
    },
);

const axiosWithAuth = axios;

export default axiosWithAuth;
