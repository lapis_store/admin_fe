import * as React from 'react';

import { Link } from 'react-router-dom';

export interface ILinkProps {
    className?: string;
    href: string;
    style?: React.CSSProperties;
    onClick?: () => any;
}

export default function LapisLink(props: React.PropsWithChildren<ILinkProps>) {
    return (
        <Link to={props.href} className={props.className} style={props.style} onClick={props.onClick}>
            {props.children}
        </Link>
    );
}
