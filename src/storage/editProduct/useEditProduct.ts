import { useContext } from 'react';
import { context } from './Provider';

export function useEditProduct() {
    // const {store, dispatchStore, Actions} = useContext(context);
    return useContext(context);
}
