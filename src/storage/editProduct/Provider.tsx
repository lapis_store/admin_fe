import * as React from 'react';

import actions, { TAction } from './actions';
import reducer, { initState, IState } from './reducer';

export const context = React.createContext<{
    store: IState;
    dispatchStore: React.Dispatch<TAction>;
    actions: typeof actions;
}>({} as any);

export interface IEditProductStoreProviderProps {}

export default function Provider(props: React.PropsWithChildren<IEditProductStoreProviderProps>) {
    const [store, dispatchStore] = React.useReducer(reducer, initState);
    return <context.Provider value={{ store, dispatchStore, actions }}>{props.children}</context.Provider>;
}
