import Handler from './Handler';
import IProductFormData from '../../share_types/form_data/admin/IProductFormData';
import IProductResponse from '../../share_types/response/admin/IProductResponse';
import { EActions, TAction } from './actions';

export interface IState {
    response: IProductResponse;
    formData: IProductFormData;
}

export const initState: IState = {
    response: {} as any,
    formData: {} as any,
};

function reducer(preState: IState, action: TAction): IState {
    const { type, payload } = action;
    switch (type) {
        case EActions.response: {
            return Handler.response(preState, payload);
        }
        case EActions.slug: {
            return Handler.slug(preState, payload);
        }
        case EActions.categoryId: {
            return Handler.categoryId(preState, payload);
        }
        case EActions.keyword: {
            return Handler.keyword(preState, payload);
        }
        case EActions.image: {
            return Handler.image(preState, payload);
        }
        case EActions.title: {
            return Handler.title(preState, payload);
        }
        case EActions.price: {
            return Handler.price(preState, payload);
        }
        case EActions.isPromotionPrice: {
            return Handler.isPromotionPrice(preState, payload);
        }
        case EActions.promotionPrice: {
            return Handler.promotionPrice(preState, payload);
        }
        case EActions.promotionStartAt: {
            return Handler.promotionStartAt(preState, payload);
        }
        case EActions.promotionEndAt: {
            return Handler.promotionEndAt(preState, payload);
        }
        case EActions.summary: {
            return Handler.summary(preState, payload);
        }
        default: {
            throw new Error(`Invalid type of action: type="${type}"`);
        }
    }
}

export default reducer;
