import actions from './actions';
import Provider, { context } from './Provider';
import { useEditProduct } from './useEditProduct';

const editProduct = {
    context,
    actions,
    Provider,
    useEditProduct,
};

export default editProduct;
