import TReducerAction from '../../share/types/TReducerAction';
import IProductResponse from '../../share_types/response/admin/IProductResponse';

export enum EActions {
    response,

    slug,
    categoryId,
    keyword,
    image,
    title,
    price,
    isPromotionPrice,
    promotionPrice,
    promotionStartAt,
    promotionEndAt,
    summary,
}

export type TAction = TReducerAction<EActions>;

const actions = {
    response(productResponse?: IProductResponse): TAction {
        return {
            type: EActions.response,
            payload: { ...productResponse },
        };
    },

    image(image: string): TAction {
        return {
            type: EActions.image,
            payload: image,
        };
    },

    slug(slug: string): TAction {
        return {
            type: EActions.slug,
            payload: slug,
        };
    },

    categoryId(categoryId: string): TAction {
        return {
            type: EActions.categoryId,
            payload: categoryId,
        };
    },

    keyword(keyword: string): TAction {
        return {
            type: EActions.keyword,
            payload: keyword,
        };
    },

    title(title: string): TAction {
        return {
            type: EActions.title,
            payload: title,
        };
    },

    price(price: number): TAction {
        return {
            type: EActions.price,
            payload: price,
        };
    },

    isPromotionPrice(isPromotionPrice: boolean): TAction {
        return {
            type: EActions.isPromotionPrice,
            payload: isPromotionPrice,
        };
    },

    promotionPrice(promotionPrice: number): TAction {
        return {
            type: EActions.promotionPrice,
            payload: promotionPrice,
        };
    },

    promotionStartAt(promotionStartAt: string): TAction {
        return {
            type: EActions.promotionStartAt,
            payload: promotionStartAt,
        };
    },

    promotionEndAt(promotionEndAt: string): TAction {
        return {
            type: EActions.promotionEndAt,
            payload: promotionEndAt,
        };
    },

    summary(summary: string): TAction {
        return {
            type: EActions.summary,
            payload: summary,
        };
    },
};

export default actions;
