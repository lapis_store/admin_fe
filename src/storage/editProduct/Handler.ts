import IProductResponse from '../../share_types/response/admin/IProductResponse';
import { IState } from './reducer';

export default class Handler {
    public static response(preState: IState, productResponse: IProductResponse): IState {
        return {
            ...preState,
            response: { ...productResponse },
            formData: { ...productResponse },
        };
    }

    public static slug(preState: IState, slug: string): IState {
        const { formData: productFormData } = preState;
        return {
            ...preState,
            formData: {
                ...productFormData,
                slug: slug,
            },
        };
    }

    public static categoryId(preState: IState, categoryId: string): IState {
        const { formData: productFormData } = preState;
        return {
            ...preState,
            formData: {
                ...productFormData,
                categoryId: categoryId,
            },
        };
    }

    public static keyword(preState: IState, keyword: string): IState {
        const { formData: productFormData } = preState;
        return {
            ...preState,
            formData: {
                ...productFormData,
                keyword: keyword,
            },
        };
    }

    public static title(preState: IState, title: string): IState {
        const { formData: productFormData } = preState;
        return {
            ...preState,
            formData: {
                ...productFormData,
                title: title,
            },
        };
    }

    public static image(preState: IState, image: string): IState {
        const { formData: productFormData } = preState;
        return {
            ...preState,
            formData: {
                ...productFormData,
                image: image,
            },
        };
    }

    public static price(preState: IState, price: number): IState {
        const { formData: productFormData } = preState;
        return {
            ...preState,
            formData: {
                ...productFormData,
                price,
            },
        };
    }

    public static isPromotionPrice(preState: IState, isPromotionalPrice: boolean): IState {
        const { formData: productFormData } = preState;
        return {
            ...preState,
            formData: {
                ...productFormData,
                isPromotionalPrice,
            },
        };
    }

    public static promotionPrice(preState: IState, promotionPrice: number): IState {
        const { formData: productFormData } = preState;
        return {
            ...preState,
            formData: {
                ...productFormData,
                promotionPrice,
            },
        };
    }

    public static promotionStartAt(preState: IState, promotionStartAt: string): IState {
        const { formData: productFormData } = preState;
        return {
            ...preState,
            formData: {
                ...productFormData,
                promotionStartAt,
            },
        };
    }

    public static promotionEndAt(preState: IState, promotionEndAt: string): IState {
        const { formData: productFormData } = preState;
        return {
            ...preState,
            formData: {
                ...productFormData,
                promotionEndAt,
            },
        };
    }

    public static summary(preState: IState, summary: string): IState {
        const { formData: productFormData } = preState;
        return {
            ...preState,
            formData: {
                ...productFormData,
                summary,
            },
        };
    }
}
