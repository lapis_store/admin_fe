import { IImageGalleryClose } from '../../components/ImageGallery/ImageGallery';
import EPages from '../../core/types/EPages';
import IGlobalEvent from '../../core/types/IGlobalEvent';
import { IMessageBox } from '../../share/components/MessageBox';
import TReducerAction from '../../share/types/TReducerAction';

export enum EActions {
    page,
    displayImageGallery,
    onImageGalleryClose,
    addEventMouseUp,
    addEventMouseMove,
    removeEventMouseUp,
    removeEventMouseMove,
    addMessageBox,
    removeMessageBox,
}

export type TAction = TReducerAction<EActions>;

const actions = {
    page(page: EPages): TAction {
        return {
            type: EActions.page,
            payload: page,
        };
    },
    displayGallery(displayImageGallery: boolean): TAction {
        return {
            type: EActions.displayImageGallery,
            payload: displayImageGallery,
        };
    },
    onImageGalleryClose(onImageGalleryClose: ((e: IImageGalleryClose) => void) | undefined): TAction {
        return {
            type: EActions.onImageGalleryClose,
            payload: onImageGalleryClose,
        };
    },
    addEventMouseUp(event: IGlobalEvent): TAction {
        return {
            type: EActions.addEventMouseUp,
            payload: event,
        };
    },
    addEventMouseMove(event: IGlobalEvent): TAction {
        return {
            type: EActions.addEventMouseMove,
            payload: event,
        };
    },
    removeEventMouseUp(_id: number): TAction {
        return {
            type: EActions.removeEventMouseUp,
            payload: _id,
        };
    },
    removeEventMouseMove(_id: number): TAction {
        return {
            type: EActions.removeEventMouseMove,
            payload: _id,
        };
    },

    addMessageBox(v: IMessageBox): TAction {
        return {
            type: EActions.addMessageBox,
            payload: v,
        };
    },

    removeMessageBox(v: string): TAction {
        return {
            type: EActions.removeMessageBox,
            payload: v,
        };
    },
};

export default actions;
