import actions from './actions';
import Provider, { context } from './Provider';
import useRootStore from './useRootStore';

const root = { actions, Provider, context, useRootStore };
export default root;
