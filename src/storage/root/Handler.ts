import { IImageGalleryClose } from '../../components/ImageGallery/ImageGallery';
import EPages from '../../core/types/EPages';
import IGlobalEvent from '../../core/types/IGlobalEvent';
import { IMessageBox } from '../../share/components/MessageBox';
import make from '../../share/functions/make';
import { IState } from './reducer';

export class Handler {
    public static page(preState: IState, page: EPages): IState {
        return {
            ...preState,
            page,
        };
    }
    public static displayImageGallery(preState: IState, displayImageGallery: boolean): IState {
        return {
            ...preState,
            displayImageGallery,
        };
    }

    public static onImageGalleryClose(
        preState: IState,
        onImageGalleryClose: ((e: IImageGalleryClose) => any) | undefined,
    ): IState {
        return {
            ...preState,
            onImageGalleryClose,
        };
    }

    public static addEventMouseUp(preState: IState, event: IGlobalEvent): IState {
        const { onMouseUp } = preState;
        return {
            ...preState,
            onMouseUp: [...onMouseUp, event],
        };
    }

    public static addEventMouseMove(preState: IState, event: IGlobalEvent): IState {
        const { onMouseMove } = preState;
        return {
            ...preState,
            onMouseMove: [...onMouseMove, event],
        };
    }

    public static removeEventMouseUp(preState: IState, _id: number): IState {
        const { onMouseUp } = preState;
        const newOnMouseUpArray = onMouseUp.filter((e) => {
            return e._id !== _id;
        });
        return {
            ...preState,
            onMouseUp: [...newOnMouseUpArray],
        };
    }

    public static removeEventMouseMove(preState: IState, _id: number): IState {
        const { onMouseMove } = preState;
        const newOnMouseMoveArray = onMouseMove.filter((e) => {
            return e._id !== _id;
        });
        return {
            ...preState,
            onMouseMove: [...newOnMouseMoveArray],
        };
    }

    public static addMessageBox(preState: IState, v: IMessageBox): IState {
        const { listMessageBox } = preState;

        const newListMessageBox: typeof listMessageBox = [
            ...listMessageBox,
            {
                id: make.id(),
                value: v,
            },
        ];

        return {
            ...preState,
            listMessageBox: newListMessageBox,
        };
    }

    public static removeMessageBox(preState: IState, id: string): IState {
        const { listMessageBox } = preState;

        const newListMessageBox = listMessageBox.filter((item) => item.id !== id);

        return {
            ...preState,
            listMessageBox: newListMessageBox,
        };
    }
}
