import * as React from 'react';
import actions, { TAction } from './actions';
import reducer, { initState, IState } from './reducer';

export const context = React.createContext<{
    store: IState;
    dispatchStore: React.Dispatch<TAction>;
    actions: typeof actions;
}>({} as any);

export interface IRootStoreProviderProps {}

export default function Provider(props: React.PropsWithChildren<IRootStoreProviderProps>) {
    const Provider = context.Provider;
    const [store, dispatchStore] = React.useReducer(reducer, initState);
    return <Provider value={{ store, dispatchStore, actions }}>{props.children}</Provider>;
}
