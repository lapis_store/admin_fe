import { IImageGalleryClose } from '../../components/ImageGallery/ImageGallery';
import EPages from '../../core/types/EPages';
import IGlobalEvent from '../../core/types/IGlobalEvent';
import { Handler } from './Handler';
import { EActions, TAction } from './actions';
import { IMessageBox } from '../../share/components/MessageBox';

export interface IState {
    page?: EPages;
    displayImageGallery?: boolean;
    onImageGalleryClose?: (e: IImageGalleryClose) => void;
    onMouseUp: IGlobalEvent[];
    onMouseMove: IGlobalEvent[];
    listMessageBox: {
        id: string;
        value: IMessageBox;
    }[];
}

export const initState: IState = {
    onMouseUp: [],
    onMouseMove: [],
    listMessageBox: [],
};

function reducer(preState: IState, action: TAction): IState {
    const { type, payload } = action;
    switch (type) {
        case EActions.page: {
            return Handler.page(preState, payload);
        }
        case EActions.displayImageGallery: {
            return Handler.displayImageGallery(preState, payload);
        }
        case EActions.onImageGalleryClose: {
            return Handler.onImageGalleryClose(preState, payload);
        }
        case EActions.addEventMouseUp: {
            return Handler.addEventMouseUp(preState, payload);
        }
        case EActions.addEventMouseMove: {
            return Handler.addEventMouseMove(preState, payload);
        }
        case EActions.removeEventMouseUp: {
            return Handler.removeEventMouseUp(preState, payload);
        }
        case EActions.removeEventMouseMove: {
            return Handler.removeEventMouseMove(preState, payload);
        }
        case EActions.addMessageBox: {
            return Handler.addMessageBox(preState, payload);
        }
        case EActions.removeMessageBox: {
            return Handler.removeMessageBox(preState, payload);
        }
        default: {
            throw new Error(`Invalid action in root_store: type of action is "${type}"`);
        }
    }
}

export default reducer;
