import editProduct from './editProduct';
import productDetail from './productDetail';
import root from './root';

const storage = {
    root,
    productDetail,
    editProduct,
};

export default storage;
