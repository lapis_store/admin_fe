import TReducerAction from '../../share/types/TReducerAction';
import IMetaTag from '../../share_types/others/IMetaTag';
import ISpecification from '../../share_types/others/ISpecification';
import IProductDetailResponse from '../../share_types/response/admin/IProductDetailResponse';
import IProductResponse from '../../share_types/response/admin/IProductResponse';

export enum EActions {
    response,
    productResponse,
    id,
    categoryId,
    metaTag,
    images,
    information,
    promotion,
    promotionMore,
    specifications,
    description,
}

export type TActions = TReducerAction<EActions>;

const actions = {
    response(v?: IProductDetailResponse): TActions {
        return {
            type: EActions.response,
            payload: { ...v },
        };
    },
    productResponse(v?: IProductResponse): TActions {
        return {
            type: EActions.productResponse,
            payload: { ...v },
        };
    },
    categoryId(v: string): TActions {
        return {
            type: EActions.categoryId,
            payload: v,
        };
    },

    metaTag(v: IMetaTag[]): TActions {
        return {
            type: EActions.metaTag,
            payload: [...v],
        };
    },
    images(v: string[]): TActions {
        return {
            type: EActions.images,
            payload: [...v],
        };
    },

    information(v: string): TActions {
        return {
            type: EActions.information,
            payload: v,
        };
    },
    promotion(v: string): TActions {
        return {
            type: EActions.promotion,
            payload: v,
        };
    },

    promotionMore(v: string): TActions {
        return {
            type: EActions.promotionMore,
            payload: v,
        };
    },
    specifications(v: ISpecification[]): TActions {
        return {
            type: EActions.specifications,
            payload: [...v],
        };
    },
    description(v: string): TActions {
        return {
            type: EActions.description,
            payload: v,
        };
    },
};

export default actions;
