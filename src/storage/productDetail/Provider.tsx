import * as React from 'react';
import actions, { TActions } from './actions';
import reducer, { initState, IState } from './reducer';

export const context = React.createContext<{
    store: IState;
    dispatchStore: React.Dispatch<TActions>;
    actions: typeof actions;
}>({} as any);

export interface IEditProductDetailStoreProviderProps {}

export default function Provider(props: React.PropsWithChildren<IEditProductDetailStoreProviderProps>) {
    const [store, dispatchStore] = React.useReducer(reducer, initState);
    return <context.Provider value={{ store, dispatchStore, actions }}>{props.children}</context.Provider>;
}
