import { useContext } from 'react';
import { context } from './Provider';

export default function useProductDetail() {
    // const { store, dispatchStore, actions } = useContext(context);
    return useContext(context);
}
