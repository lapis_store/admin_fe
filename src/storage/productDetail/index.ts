import useProductDetail from './useProductDetail';
import Provider, { context } from './Provider';
import actions from './actions';

const productDetail = {
    context,
    useProductDetail,
    Provider,
    actions,
};

export default productDetail;
