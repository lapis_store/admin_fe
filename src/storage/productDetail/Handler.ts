import IMetaTag from '../../share_types/others/IMetaTag';
import ISpecification from '../../share_types/others/ISpecification';
import IProductDetailResponse from '../../share_types/response/admin/IProductDetailResponse';
import IProductResponse from '../../share_types/response/admin/IProductResponse';
import { IState } from './reducer';

class Handler {
    public static response(preState: IState, payload: IProductDetailResponse): IState {
        const { productResponse } = preState;
        return {
            productResponse,
            formData: { ...payload },
            response: { ...payload },
        };
    }

    public static productResponse(preState: IState, payload: IProductResponse): IState {
        const { response, formData } = preState;
        return {
            formData,
            response,
            productResponse: { ...payload },
        };
    }

    public static metaTag(preState: IState, payload: IMetaTag[]): IState {
        const { response, productResponse, formData } = preState;
        return {
            response,
            productResponse,
            formData: {
                ...formData,
                metaTag: [...payload],
            },
        };
    }

    public static images(preState: IState, payload: string[]): IState {
        const { response, productResponse, formData } = preState;
        return {
            response,
            productResponse,
            formData: {
                ...formData,
                images: [...payload],
            },
        };
    }

    public static information(preState: IState, payload: string): IState {
        const { response, productResponse, formData } = preState;
        return {
            response,
            productResponse,
            formData: {
                ...formData,
                information: payload,
            },
        };
    }

    public static promotion(preState: IState, payload: string): IState {
        const { response, productResponse, formData } = preState;
        return {
            response,
            productResponse,
            formData: {
                ...formData,
                promotion: payload,
            },
        };
    }

    public static promotionMore(preState: IState, payload: string): IState {
        const { response, productResponse, formData } = preState;
        return {
            response,
            productResponse,
            formData: {
                ...formData,
                promotionMore: payload,
            },
        };
    }

    public static specifications(preState: IState, payload: ISpecification[]): IState {
        const { response, productResponse, formData } = preState;
        return {
            response,
            productResponse,
            formData: {
                ...formData,
                specifications: [...payload],
            },
        };
    }

    public static description(preState: IState, payload: string): IState {
        const { response, productResponse, formData } = preState;
        return {
            response,
            productResponse,
            formData: {
                ...formData,
                description: payload,
            },
        };
    }

    public static id(preState: IState, payload: string): IState {
        const { response, productResponse, formData } = preState;
        return {
            response,
            productResponse,
            formData: {
                ...formData,
                _id: payload,
            },
        };
    }
}
export default Handler;
