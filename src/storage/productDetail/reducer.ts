import IProductDetailFormData from '../../share_types/form_data/admin/IProductDetailFormData';
import IProductDetailResponse from '../../share_types/response/admin/IProductDetailResponse';
import IProductResponse from '../../share_types/response/admin/IProductResponse';
import { TActions, EActions } from './actions';
import Handler from './Handler';

export interface IState {
    response: IProductDetailResponse;
    productResponse: IProductResponse;
    formData: IProductDetailFormData;
}

export const initState: IState = {
    response: {} as any,
    productResponse: {} as any,
    formData: {},
};

export default function reducer(preState: IState, action: TActions): IState {
    const { type, payload } = action;

    switch (type) {
        case EActions.response: {
            return Handler.response(preState, payload);
        }
        case EActions.productResponse: {
            return Handler.productResponse(preState, payload);
        }
        case EActions.id: {
            return Handler.id(preState, payload);
        }
        case EActions.metaTag: {
            return Handler.metaTag(preState, payload);
        }
        case EActions.images: {
            return Handler.images(preState, payload);
        }
        case EActions.information: {
            return Handler.information(preState, payload);
        }
        case EActions.promotion: {
            return Handler.promotion(preState, payload);
        }
        case EActions.promotionMore: {
            return Handler.promotionMore(preState, payload);
        }
        case EActions.specifications: {
            return Handler.specifications(preState, payload);
        }
        case EActions.description: {
            return Handler.description(preState, payload);
        }
        default: {
            throw new Error(`Invalid action: Type of action = "${type}"`);
        }
    }
}
