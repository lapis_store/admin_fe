export default class ENV {
    public static readonly API_HOST: string = 'http://service-lapisstore.lapisblog.com';
    public static readonly STATIC_HOST: string = 'http://static-lapisstore.lapisblog.com';
}
