import ICategoryResponse from '../../share_types/response/admin/category/ICategoryResponse';

export default function makeCategoryPath(categories: ICategoryResponse[], category: ICategoryResponse): string {
    if (!category.parentId) {
        return category.title;
    }

    const getParentPath = (v: ICategoryResponse): string => {
        if (!v.parentId) return v.title;

        const parent = categories.find((item) => {
            if (!v.parentId) return false;
            return item._id.toString() === v.parentId.toString();
        });

        if (!parent) return v.title;

        return `${getParentPath(parent)}/${v.title}`;
    };

    const path = `${getParentPath(category)}`;
    return path;
}
