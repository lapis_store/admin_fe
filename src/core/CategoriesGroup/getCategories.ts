import axios from 'axios';
import ENV from '../ENV';
import ICategory from '../types/ICategory';

export default async function getCategories(): Promise<ICategory[] | undefined> {
    try {
        const res = await axios({
            url: `${ENV.API_HOST}/admin/category/list`,
            method: 'GET',
            data: {
                name: 'tuan nguyen',
            },
        });

        if (res.status !== 200) {
            return undefined;
        }

        return res.data;
    } catch (e) {
        return undefined;
    }
}
