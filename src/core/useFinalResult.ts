function useFinalResult(onStop: (...v: any) => any, timeDelay: number = 1000) {
    console.log('ok');

    let isReady: boolean = true;

    let values: any = [];

    let preCount: number = 0;
    let count: number = 0;

    return (...args: any) => {
        console.log('call');

        count += 1;
        values = args;

        if (!isReady) return;

        isReady = false;
        const timerId = setInterval(() => {
            console.log('checking');

            if (preCount !== count) {
                preCount = count;
                return;
            }

            clearInterval(timerId);

            // dispatch event
            onStop(...values);

            preCount = 0;
            count = 0;
            isReady = true;
        }, timeDelay);
    };
}
export default useFinalResult;
