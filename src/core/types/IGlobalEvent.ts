interface IGlobalEvent {
    _id: number;
    handler: (e?: any) => any;
}

export default IGlobalEvent;
