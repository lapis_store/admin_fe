export enum EValidationIcon {
    validate = 'check_circle',
    noValidate = 'error_outline',
    warning = 'warning_amber',
    info = 'info',
}

export enum EValidationIconColor {
    validate = 'rgb(0,255,0)',
    noValidate = 'rgb(255,0,0)',
    warning = 'rgb(255,255,0)',
    info = 'rgb(0,0,255)',
}
