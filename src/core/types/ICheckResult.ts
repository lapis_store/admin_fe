enum ECheckResultStatus {
    pass = 2,
    info = 1,
    warning = 0,
    fail = -1,
}

interface ICheckResult {
    message: string;
    status: ECheckResultStatus;
}

export { ECheckResultStatus };
export default ICheckResult;
