enum EPages {
    productManagement = 'product-management',
    categoryManagement = 'category-management',
    homeManagement = 'home-management',
    orderManagement = 'order-management',

    editProductCard = 'edit-product-card',
    editProductDetail = 'edit-product-detail',
    login = 'login',

    default = '',
}

export default EPages;
