import { EValidationIcon, EValidationIconColor } from './EValidation';

interface IInputIconState {
    icon?: EValidationIcon;
    iconColor?: EValidationIconColor;
    display?: boolean;
}

export default IInputIconState;
