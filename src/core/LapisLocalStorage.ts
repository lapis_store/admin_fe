enum ELocalStorageKey {
    accessToken = 'accessToken',
    setting = 'setting',
}

interface ISetting {
    notShowMessageAllowAccess: boolean;
}

export default class LapisLocalStorage {
    private static _instance: LapisLocalStorage | undefined = undefined;

    private _setting: ISetting = {
        notShowMessageAllowAccess: false,
    };

    private _accessToken: string = '';

    public static get instance() {
        if (this._instance === undefined) {
            this._instance = new LapisLocalStorage();
        }
        return this._instance;
    }

    // accessToken
    public get accessToken() {
        return this._accessToken;
    }

    public set accessToken(v: string) {
        this._accessToken = v;
        localStorage.setItem('accessToken', this._accessToken);
    }

    // setting
    public get setting() {
        return this._setting;
    }

    public set setting(v: Partial<ISetting>) {
        this._setting = {
            ...this._setting,
            ...v,
        };
        localStorage.setItem(ELocalStorageKey.setting, JSON.stringify(this._setting));
    }

    private constructor() {
        this.loadAll();
    }

    public saveSetting = () => {
        localStorage.setItem(ELocalStorageKey.setting, JSON.stringify(this._setting));
    };

    private loadAll() {
        this._accessToken = localStorage.getItem(ELocalStorageKey.accessToken) || '';

        this.loadSetting();
    }

    private loadSetting() {
        const strSetting = localStorage.getItem(ELocalStorageKey.setting);
        if (!strSetting) {
            localStorage.setItem(ELocalStorageKey.setting, JSON.stringify(this._setting));
            return;
        }

        try {
            const setting: ISetting = JSON.parse(strSetting);
            this._setting = {
                ...this._setting,
                ...setting,
            };
        } catch {
            localStorage.setItem(ELocalStorageKey.setting, JSON.stringify(this._setting));
        }
    }
}
