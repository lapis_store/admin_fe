import * as React from 'react';
import MessageBox from '../../../share/components/MessageBox';
import storage from '../../../storage';

export interface IListMessageBoxProps {}

export default function ListMessageBox(props: IListMessageBoxProps) {
    const { store, actions, dispatchStore } = storage.root.useRootStore();
    const { listMessageBox } = store;

    if (listMessageBox.length === 0) return <></>;

    const handlerMessageBoxClose = (id?: string) => {
        if (!id) {
            console.log('A MessageBox have id is undefined');
            return;
        }
        dispatchStore(actions.removeMessageBox(id));
    };

    const messageBoxElmnts = listMessageBox.map((item, i) => {
        const { id, value } = item;
        return (
            <MessageBox
                key={id}
                id={id}
                title={value.title}
                buttons={value.buttons}
                icon={value.icon}
                message={value.message}
                backgroundColor={value.backgroundColor || 'rgba(0,50,50,0.95)'}
                onClose={handlerMessageBoxClose}
                index={i}
            />
        );
    });

    return <>{messageBoxElmnts}</>;
}
