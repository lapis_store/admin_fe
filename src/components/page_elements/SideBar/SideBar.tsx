import * as React from 'react';
import make from '../../../share/functions/make';

import Logo from './Logo';
import Navigation from './Navigation';

import styles from './SideBar.module.scss';

export interface ISideBarProps {
    className?: string;
}

export default function SideBar(props: ISideBarProps) {
    return (
        <div className={make.className([styles['side-bar'], props.className])}>
            <Logo />
            <Navigation />
        </div>
    );
}
