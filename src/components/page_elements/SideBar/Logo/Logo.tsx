import * as React from 'react';
import make from '../../../../share/functions/make';

import styles from './Logo.module.scss';

export interface ILogoProps {}

export default function Logo(props: ILogoProps) {
    return (
        <div className={make.className([styles['logo']])}>
            <span>Lapis</span>
            <span>Store</span>
        </div>
    );
}
