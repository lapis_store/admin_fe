import EPages from '../../../../core/types/EPages';

type TNavItem = {
    title: string;
    link: EPages;
    icon: string;
};

const navItems: TNavItem[] = [
    {
        title: 'Dashboard',
        link: EPages.default,
        icon: 'dashboard',
    },
    {
        title: 'Đơn hàng',
        link: EPages.orderManagement,
        icon: 'receipt_long',
    },
    {
        title: 'Sản phẩm',
        link: EPages.productManagement,
        icon: 'view_in_ar',
    },
    {
        title: 'Danh mục',
        link: EPages.categoryManagement,
        icon: 'category',
    },
    {
        title: 'Banner',
        link: EPages.homeManagement,
        icon: 'home',
    },
];
export default navItems;
