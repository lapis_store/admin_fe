import * as React from 'react';
import { Link } from 'react-router-dom';
import EPages from '../../../../core/types/EPages';
import make from '../../../../share/functions/make';
import storage from '../../../../storage';

import styles from './Navigation.module.scss';
import navItems from './navItems';

export interface INavigationProps {}

export default function Navigation(props: INavigationProps) {
    const { store } = storage.root.useRootStore();
    const { page } = store;

    const navItemsElmnts = navItems.map((item, i) => {
        return (
            <li key={`${item.link}_${i}`} className={make.className([page === item.link && 'selected'], styles)}>
                <Link to={`/${item.link}`}>
                    <strong>{item.icon}</strong>
                    <span>{item.title}</span>
                </Link>
            </li>
        );
    });

    return (
        <div className={make.className([styles['navigation']])}>
            <ul>{navItemsElmnts}</ul>
        </div>
    );
}
