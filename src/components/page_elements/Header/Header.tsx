import * as React from 'react';
import { useNavigate } from 'react-router-dom';
import LapisLocalStorage from '../../../core/LapisLocalStorage';
import EPages from '../../../core/types/EPages';
import LapisCircularButton from '../../../share/components/LapisCircularButton';
import LapisSearch from '../../../share/components/LapisSearch/LapisSearch';
import make from '../../../share/functions/make';
import storage from '../../../storage';

// import LapisSearch from '../../LapisUi/LapisSearch/LapisSearch';

import styles from './Header.module.scss';

export interface IHeaderProps {
    className?: string;
}

export default function Header(props: IHeaderProps) {
    const { dispatchStore, actions } = storage.root.useRootStore();
    const navigate = useNavigate();

    const handlerButtonLogoutClick = React.useCallback(() => {
        LapisLocalStorage.instance.accessToken = '';
        navigate(`/${EPages.login}`);
    }, [navigate]);

    const handlerImageGalleryButtonClick = React.useCallback(() => {
        dispatchStore(actions.displayGallery(true));
    }, [dispatchStore, actions]);

    return (
        <div className={make.className([styles['header'], props.className])}>
            <div>
                <LapisSearch />
            </div>
            <div className={styles['buttons']}>
                <LapisCircularButton
                    className={styles['button']}
                    icon='photo_library'
                    tooltip='Image Gallery'
                    onClick={handlerImageGalleryButtonClick}
                />
                <LapisCircularButton
                    className={styles['button']}
                    icon='logout'
                    tooltip='Đăng xuất'
                    onClick={handlerButtonLogoutClick}
                />
            </div>
        </div>
    );
}
