import * as React from 'react';
import make from '../../../share/functions/make';
import setCssVariables from '../../../share/functions/setCssVariables';
import Header from './components/Header';
import List from './components/List';
import FormUpload from './components/FormUpload';
import { IFormUploadRef } from './components/FormUpload/FormUpload';

import styles from './ImageGalleryForm.module.scss';
import storage from '../../../storage';
import api from '../../../api/api';
import { IUploadImageResponse } from '../../../api/staticHost/upload';
import useMessageBox from '../../../hooks/userMessageBox';
import { IStatusResponse } from '../../../api/staticHost/status';
export interface IImageGalleryFormProps {
    display?: boolean;
    onButtonCloseClick?: () => any;
}

export interface IImageGalleryFormRef {
    getSelectedImages: () => string[];
    setSelectedImages: (v: string[]) => void;
}

function ImageGalleryForm(props: IImageGalleryFormProps, ref: React.RefObject<IImageGalleryFormRef> & any) {
    const { dispatchStore, actions } = storage.root.useRootStore();

    const maxNumberOfMessage = 10;

    const [images, setImages] = React.useState<string[] | undefined>(undefined);
    const [isDragEnter, setIsDragEnter] = React.useState<boolean>(false);
    const [messages, setMessages] = React.useState<string[]>([]);
    const [selectedImages, setSelectedImages] = React.useState<string[]>([]);
    const [imagesStatus, setImagesStatus] = React.useState<IStatusResponse>({
        lastUpdate: Date.now(),
        processing: 0,
    });

    const imagesStatusRef = React.useRef<IStatusResponse>({
        lastUpdate: 0,
        processing: 0,
    });

    const isDisplay = React.useRef<boolean>(false);
    const imageGalleryFormRef = React.useRef<HTMLDivElement>(null);
    const imageGalleryFormUploadRef = React.useRef<IFormUploadRef>(null);
    const isIntervalRunning = React.useRef<boolean>(false);
    const dragStatus = React.useRef<number>(0);
    const position = React.useRef<{
        mouse: [number, number];
        form: [number, number];
        isMouseDown: boolean;
    }>({
        mouse: [0, 0],
        form: [0, 0],
        isMouseDown: false,
    });

    isDisplay.current = props.display ? true : false;

    const messageBox = useMessageBox();

    const addMessage = React.useCallback((v: string) => {
        setMessages((preState) => {
            const d = new Date();
            const h = d.getHours().toString().padStart(2, '0');
            const m = d.getMinutes().toString().padStart(2, '0');
            const s = d.getSeconds().toString().padStart(2, '0');
            const newMessage = `${h}:${m}:${s} ${v}`;
            const newState = [newMessage, ...preState];

            if (newState.length > maxNumberOfMessage) {
                return newState.slice(0, maxNumberOfMessage);
            }

            return newState;
        });
    }, []);

    React.useImperativeHandle(
        ref,
        () =>
            ({
                getSelectedImages: () => {
                    return [...selectedImages];
                },
                setSelectedImages: (v) => {
                    setSelectedImages([...v]);
                },
            } as IImageGalleryFormRef),
    );

    const imagesSort = React.useMemo(() => {
        if (!images || images.length === 0) {
            return undefined;
        }

        const tmp = [...images];

        return tmp.sort(function (a: string, b: string) {
            const imageIdA = a.split('_')[1];
            const imageIdB = b.split('_')[1];

            if (imageIdA > imageIdB) return -1;
            if (imageIdA < imageIdB) return 1;
            return 0;
        });
    }, [images]);

    const loadImages = React.useCallback(async () => {
        addMessage('Đang đồng bộ hóa');

        const resImages = await api.staticHost.list();

        setImages(resImages);

        setSelectedImages((preState) => {
            if (preState.length === 0 || !resImages || resImages.length === 0) return preState;

            const newState = preState.filter((elmnt) => {
                return resImages?.includes(elmnt);
            });
            return newState;
        });

        addMessage(`Đã đồng bộ hoá`);
    }, [addMessage]);

    const handlerUploading = React.useCallback(
        (imageNames: string[]) => {
            addMessage(`Đang tải lên và xử lý ${imageNames.length} tệp`);
        },
        [addMessage],
    );

    const handlerUploaded = React.useCallback(
        (res: IUploadImageResponse) => {
            if (res.status === 'failed') {
                addMessage('Tải lên thất bại');
                messageBox.show({
                    title: 'Thông báo',
                    icon: 'error',
                    message: res.message,
                    buttons: [{ label: 'Ok' }],
                });
                return;
            }

            addMessage('Đã tải lên thành công');
        },
        [addMessage, messageBox],
    );

    const handlerRemoveClick = React.useCallback(
        async (imageName: string) => {
            const res = await api.staticHost.remove(imageName);
            if (!res || res.status === 'failure') {
                messageBox.show({
                    title: 'Thông báo',
                    icon: 'error',
                    message: `Xóa ${imageName} thất bại`,
                    buttons: [{ label: 'Ok' }],
                });
                return;
            }
            loadImages();
        },
        [messageBox, loadImages],
    );

    const handlerDragEnter = React.useCallback(() => {
        if (!imageGalleryFormRef.current) return;
        setCssVariables(imageGalleryFormRef, [
            {
                property: '--image-gallery-height',
                value: `${imageGalleryFormRef.current.offsetHeight}px`,
            },
        ]);
    }, []);

    const handlerDragOver = React.useCallback(() => {
        dragStatus.current = 1;

        if (isIntervalRunning.current) return;
        setIsDragEnter(true);

        handlerDragEnter();

        isIntervalRunning.current = true;
        const intervalId = setInterval(() => {
            if (dragStatus.current === 0) {
                isIntervalRunning.current = false;
                setIsDragEnter(false);
                clearInterval(intervalId);
                return;
            }
            dragStatus.current = 0;
        }, 500);
    }, [handlerDragEnter]);

    const handlerUploadButtonClick = React.useCallback(() => {
        if (!imageGalleryFormUploadRef.current) return;
        imageGalleryFormUploadRef.current.uploadFile();
    }, []);

    const handlerSyncButtonClick = React.useCallback(() => {
        loadImages();
    }, [loadImages]);

    const handlerImageClick = React.useCallback((imageName: string) => {
        setSelectedImages((preState) => {
            if (preState.includes(imageName)) {
                return preState.filter((elmnt) => {
                    return imageName !== elmnt;
                });
            }
            return [...preState, imageName];
        });
    }, []);

    const handlerGlobalMouseUp = React.useCallback(() => {
        position.current.isMouseDown = false;
        dispatchStore(actions.removeEventMouseUp(100));
        dispatchStore(actions.removeEventMouseMove(100));
    }, [dispatchStore, actions]);

    const handlerGlobalMouseMove = React.useCallback((e: MouseEvent) => {
        if (!position.current.isMouseDown) return;
        if (!imageGalleryFormRef.current) return;

        const [mouseX, mouseY] = position.current.mouse;
        const [formX, formY] = position.current.form;

        const x: number = e.clientX - mouseX;
        const y: number = e.clientY - mouseY;

        imageGalleryFormRef.current.style.left = `${formX + x}px`;
        imageGalleryFormRef.current.style.top = `${formY + y}px`;
    }, []);

    const handlerHeaderMouseDown = React.useCallback(
        (e: React.MouseEvent<HTMLDivElement>) => {
            if (!imageGalleryFormRef.current) return;

            position.current.mouse = [e.clientX, e.clientY];
            position.current.form = [imageGalleryFormRef.current.offsetLeft, imageGalleryFormRef.current.offsetTop];
            position.current.isMouseDown = true;

            dispatchStore(
                actions.addEventMouseUp({
                    _id: 100,
                    handler: handlerGlobalMouseUp,
                }),
            );

            dispatchStore(
                actions.addEventMouseMove({
                    _id: 100,
                    handler: handlerGlobalMouseMove,
                }),
            );
        },
        [dispatchStore, handlerGlobalMouseUp, handlerGlobalMouseMove, actions],
    );

    React.useEffect(() => {
        loadImages();
    }, [loadImages]);

    React.useEffect(() => {
        let isUpdating = false;
        const timerId = setInterval(async () => {
            if (isUpdating) return;
            if (!isDisplay.current) return;

            isUpdating = true;
            const res = await api.staticHost.status();
            if (!res) return;

            if (
                res.lastUpdate !== imagesStatusRef.current.lastUpdate ||
                res.processing !== imagesStatusRef.current.processing
            ) {
                loadImages();
                setImagesStatus({
                    lastUpdate: res.lastUpdate,
                    processing: res.processing,
                });
            }

            imagesStatusRef.current = res;
            isUpdating = false;
        }, 2000);

        return () => {
            clearInterval(timerId);
        };
    }, [loadImages]);

    return (
        <div
            ref={imageGalleryFormRef}
            className={make.className(['image-gallery-form'], styles)}
            onDragOver={handlerDragOver}
        >
            <Header
                messages={messages}
                processing={imagesStatus.processing}
                lastUpdate={imagesStatus.lastUpdate}
                onMouseDown={handlerHeaderMouseDown}
                onUploadButtonClick={handlerUploadButtonClick}
                onSyncButtonClick={handlerSyncButtonClick}
                onCloseButtonClick={props.onButtonCloseClick}
            />
            <FormUpload
                ref={imageGalleryFormUploadRef}
                onUploaded={handlerUploaded}
                onUploading={handlerUploading}
                display={isDragEnter}
            />
            <List
                images={imagesSort}
                imagesSelected={selectedImages}
                onRemoveClick={handlerRemoveClick}
                onImageClick={handlerImageClick}
            />
        </div>
    );
}

export default React.forwardRef<IImageGalleryFormRef, IImageGalleryFormProps>(ImageGalleryForm);
