import * as React from 'react';
import api from '../../../../../../api/api';
import make from '../../../../../../share/functions/make';

import styles from './Item.module.scss';

export interface IItemProps {
    className?: string;
    selected?: boolean;
    imageFileName: string;
    onRemoveClick?: (v: string) => any;
    onClick?: (v: string) => any;
}

export default function Item(props: IItemProps) {
    const { imageFileName, onRemoveClick, onClick } = props;
    const [fileName, extension] = imageFileName.split('.');

    let imageName = '';
    let ratio = '1';
    const tmp = fileName.split('_');
    if (tmp.length === 3) {
        imageName = tmp[0].replaceAll('-', ' ');
        ratio = tmp[2];
    }

    const inputRef = React.useRef<HTMLInputElement>(null);

    const [readOnly, setReadonly] = React.useState<boolean>(true);
    const [inputValue, setInputValue] = React.useState<string>(imageName);

    const handlerInputDoubleClick = () => {
        setReadonly(false);
    };

    const handlerInputChange = (e: React.FormEvent<HTMLInputElement>) => {
        setInputValue(e.currentTarget.value);
    };

    const handlerInputBlur = async (e: React.FormEvent<HTMLInputElement>) => {
        setReadonly(true);
        setInputValue(imageName);
        await api.staticHost.rename(imageFileName, e.currentTarget.value);
    };

    const handlerImageClick = () => {
        if (onClick) onClick(imageFileName);
    };

    const handlerRemoveClick = () => {
        if (onRemoveClick) onRemoveClick(imageFileName);
    };

    const handlerCopyLinkClick = () => {
        alert(`${imageFileName}`);
    };

    React.useEffect(() => {
        if (readOnly) return;
        if (!inputRef.current) return;
        inputRef.current.focus();
    }, [readOnly]);

    return (
        <div
            className={make.className([
                styles['item'],
                props.selected && styles['selected'],
                readOnly || styles['is-changing-name'],
                props.className,
            ])}
            style={
                {
                    backgroundImage: `url('${make.imageAddress(imageFileName, 'thumbnail')}')`,
                    '--aspect-ratio': ratio.replace('x', '/'),
                } as any
            }
        >
            <div className={styles['layer-wrap']}>
                <div className={styles['layer']}>
                    <button className={make.className(['btn', 'btn-delete'], styles)} onClick={handlerRemoveClick}>
                        delete
                    </button>
                    <button className={make.className(['btn', 'btn-copy-link'], styles)} onClick={handlerCopyLinkClick}>
                        link
                    </button>
                </div>
            </div>

            <div className={styles['layer-wrap']}>
                <div className={styles['layer']}>
                    <div className={make.className(['extension', extension], styles)}>{extension}</div>
                </div>
            </div>

            <div className={styles['layer-wrap']}>
                <div className={styles['layer']}>
                    <input
                        ref={inputRef}
                        type={'text'}
                        className={styles['image-name']}
                        value={inputValue}
                        readOnly={readOnly}
                        onDoubleClick={handlerInputDoubleClick}
                        onChange={handlerInputChange}
                        onBlur={handlerInputBlur}
                    />
                </div>
            </div>

            <div className={styles['layer-wrap']}>
                <div className={styles['layer']}>
                    <div className={styles['selection-area']} onClick={handlerImageClick} />
                </div>
            </div>
        </div>
    );
}
