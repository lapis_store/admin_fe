import * as React from 'react';
import make from '../../../../../share/functions/make';
import TLapisReactElements from '../../../../../share/types/TLapisReactElements';
import Item from './Item';

import styles from './List.module.scss';

export interface IListProps {
    images?: string[];
    imagesSelected?: string[];
    onRemoveClick?: (v: string) => any;
    onImageClick?: (v: string) => any;
}

export default function List(props: IListProps) {
    const { images, imagesSelected, onRemoveClick, onImageClick } = props;

    const itemElmnts: TLapisReactElements = React.useMemo(() => {
        if (!images || images.length === 0) return undefined;
        return images.map((image, i) => {
            const selected = imagesSelected?.includes(image);
            return (
                <Item
                    key={image}
                    selected={selected}
                    imageFileName={image}
                    onRemoveClick={onRemoveClick}
                    onClick={onImageClick}
                />
            );
        });
    }, [images, onRemoveClick, onImageClick, imagesSelected]);

    return <div className={styles['list']}>{itemElmnts}</div>;
}
