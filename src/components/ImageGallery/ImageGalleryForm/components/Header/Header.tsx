import * as React from 'react';
import LapisCircularButton from '../../../../../share/components/LapisCircularButton';
import make from '../../../../../share/functions/make';

import styles from './Header.module.scss';

export interface IHeaderProps {
    messages?: string[];
    processing?: number;
    lastUpdate?: number;

    onMouseDown?: (e: React.MouseEvent<HTMLDivElement>) => any;
    onMouseUp?: (e: React.MouseEvent<HTMLDivElement>) => any;
    onMouseMove?: (e: React.MouseEvent<HTMLDivElement>) => any;

    onUploadButtonClick?: () => any;
    onSyncButtonClick?: () => any;
    onCloseButtonClick?: () => any;
}

export default function Header(props: IHeaderProps) {
    const { onUploadButtonClick, onSyncButtonClick, onCloseButtonClick, onMouseDown, onMouseUp, onMouseMove } = props;

    const message = props.messages?.map((item, i) => {
        return <li key={`${i}_${item}`}>{item}</li>;
    });

    const lastUpdate: string = React.useMemo(() => {
        if (!props.lastUpdate) return '';
        const d = new Date(props.lastUpdate);

        const dd = d.getDate().toString().padStart(2, '0');
        const mm = d.getMonth().toString().padStart(2, '0');
        const yyyy = d.getFullYear().toString();

        const h = d.getHours().toString().padStart(2, '0');
        const m = d.getMinutes().toString().padStart(2, '0');
        const s = d.getSeconds().toString().padStart(2, '0');

        return `${dd}-${mm}-${yyyy} ${h}:${m}:${s}`;
    }, [props.lastUpdate]);

    return (
        <div className={styles['header']} onMouseDown={onMouseDown} onMouseUp={onMouseUp} onMouseMove={onMouseMove}>
            <div className={styles['left-side']}>
                <div className={styles['icon']}></div>
                <div className={styles['text']}>ImageGallery</div>
            </div>

            <div className={styles['message']}>
                <ul>{message}</ul>
            </div>

            <div className={styles['right-side']}>
                <div className={styles['status']}>
                    <div className={make.className(['item', 'last-update'], styles)}>
                        <span>Cập nhật lần cuối</span>
                        <span>{lastUpdate}</span>
                    </div>
                    <div className={make.className(['item', 'currently-processing'], styles)}>
                        <span>Đang được xử lý</span>
                        <span>{props.processing}</span>
                    </div>
                </div>

                <div className={styles['buttons']}>
                    <LapisCircularButton
                        className={styles['btn']}
                        icon='cloud_sync'
                        tooltip='Đồng bộ hoá'
                        onClick={onSyncButtonClick}
                    />
                    <LapisCircularButton
                        className={styles['btn']}
                        icon='file_upload'
                        tooltip='Tải lên'
                        onClick={onUploadButtonClick}
                    />
                    <LapisCircularButton className={styles['btn']} icon='close' onClick={onCloseButtonClick} />
                </div>
            </div>
        </div>
    );
}
