import * as React from 'react';
import api from '../../../../../api/api';
import { IUploadImageResponse } from '../../../../../api/staticHost/upload';
import useMessageBox from '../../../../../hooks/userMessageBox';
import make from '../../../../../share/functions/make';

import styles from './FormUpload.module.scss';

export interface IFormUploadProps {
    display?: boolean;
    onUploading?: (imageNames: string[]) => any;
    onUploaded?: (res: IUploadImageResponse) => any;
}

export interface IFormUploadRef {
    uploadFile: () => void;
}

function FormUpload(props: IFormUploadProps, ref: React.RefObject<IFormUploadRef> & any) {
    const { onUploaded, onUploading } = props;

    const messageBox = useMessageBox();

    const inputRef = React.useRef<HTMLInputElement>(null);

    React.useImperativeHandle(
        ref,
        () =>
            ({
                uploadFile: () => {
                    if (!inputRef.current) return;
                    inputRef.current.click();
                    console.log('ok');
                },
            } as IFormUploadRef),
    );

    const uploadFile = React.useCallback(async () => {
        if (!inputRef.current) return;

        if (!inputRef.current.files) {
            messageBox.show({
                title: 'Thông báo',
                icon: 'info',
                message: 'Không có file nào được tải lên',
                buttons: [{ label: 'Ok' }],
            });
            return;
        }

        if (!window.FormData) {
            messageBox.show({
                title: 'Thông báo',
                icon: 'info',
                message:
                    'Trình duyệt này không hỗ trợ FormData. Hãy sử dụng trình duyệt chrome để có trải nghiệm tốt hơn',
                buttons: [{ label: 'Ok' }],
            });
            return;
        }

        const files = inputRef.current.files;
        const formData = new FormData();
        const imageNames: string[] = [];

        for (let i = 0; i < files.length; i++) {
            formData.append('images', files[i]);
            imageNames.push(files[i].name);
        }

        // dispatch event
        if (onUploading) onUploading(imageNames);

        const res = await api.staticHost.upload(formData);

        // dispatch event
        if (onUploaded) onUploaded(res);

        // clear input-file value
        inputRef.current.value = '';
    }, [onUploading, onUploaded, messageBox]);

    const handlerInputChange = React.useCallback(() => {
        // avoid file not ready
        setTimeout(() => {
            uploadFile();
        }, 200);
    }, [uploadFile]);

    return (
        <div className={make.className(['form-upload', props.display && 'display'], styles)}>
            <form encType='multipart/form-data' method='post'>
                <div className={styles['background']}>
                    <div className={styles['message']}>
                        <span>Thả ảnh vào đây để tải lên</span>
                    </div>
                </div>
                <input ref={inputRef} type='file' name='images' multiple onChange={handlerInputChange} />
            </form>
        </div>
    );
}

export default React.forwardRef<IFormUploadRef, IFormUploadProps>(FormUpload);
