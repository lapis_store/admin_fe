import * as React from 'react';
import LapisCover from '../../share/components/LapisCover';
import make from '../../share/functions/make';
import storage from '../../storage';
import styles from './ImageGallery.module.scss';
import ImageGalleryForm from './ImageGalleryForm';
import { IImageGalleryFormRef } from './ImageGalleryForm/ImageGalleryForm';

export interface IImageGalleryClose {
    dialogResult: 'ok' | 'close';
    selectedImages: string[];
}

export interface IImageGalleryProps {}

export default function ImageGallery(props: IImageGalleryProps) {
    const { store, dispatchStore, actions } = storage.root.useRootStore();
    const { displayImageGallery, onImageGalleryClose } = store;

    const imageGalleryFormRef = React.useRef<IImageGalleryFormRef>(null);

    const handlerClose = React.useCallback(() => {
        if (!imageGalleryFormRef.current) return;
        // dispatch event
        if (onImageGalleryClose) {
            onImageGalleryClose({
                dialogResult: 'close',
                selectedImages: imageGalleryFormRef.current.getSelectedImages(),
            });
            dispatchStore(actions.onImageGalleryClose(undefined));
        }

        dispatchStore(actions.displayGallery(false));
    }, [dispatchStore, onImageGalleryClose, actions]);

    React.useEffect(() => {
        if (!imageGalleryFormRef.current) return;
        imageGalleryFormRef.current.setSelectedImages([]);
    }, [displayImageGallery]);

    return (
        <div className={make.className(['image-gallery', !displayImageGallery && 'hide'], styles)}>
            <LapisCover onClick={handlerClose} />
            <ImageGalleryForm
                ref={imageGalleryFormRef}
                display={displayImageGallery}
                onButtonCloseClick={handlerClose}
            />
        </div>
    );
}
