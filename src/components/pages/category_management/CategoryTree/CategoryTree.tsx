import * as React from 'react';
import api from '../../../../api/api';
import useMessageBox from '../../../../hooks/userMessageBox';
import ICategoryFormData from '../../../../share_types/form_data/admin/ICategoryFormData';
import ICategoryResponse from '../../../../share_types/response/admin/category/ICategoryResponse';
import CategoryItem from '../CategoryItem';
import ICategoryTreeItem from '../types/ICategoryTreeItem';

import styles from './CategoryTree.module.scss';

export const CategoryTreeContext = React.createContext<{
    loadCategories: () => Promise<void>;
    idCategoryNewest: string | undefined;
    setIdCategoryNewest: React.Dispatch<React.SetStateAction<string | undefined>>;
    setCategories: React.Dispatch<React.SetStateAction<ICategoryResponse[] | undefined>>;
}>({} as any);

export interface ICategoryTreeProps {}

export default function CategoryTree(props: ICategoryTreeProps) {
    const messageBox = useMessageBox();

    const [categories, setCategories] = React.useState<ICategoryResponse[] | undefined>();
    const [idCategoryNewest, setIdCategoryNewest] = React.useState<string | undefined>(undefined);

    const loadCategories = async () => {
        const resData = await api.category.list();
        setCategories(resData);
    };

    const handlerBtnAddClick = React.useCallback(async () => {
        const formData: ICategoryFormData = {
            index: 0,
            title: 'Danh mục mới',
            parentId: undefined,
            image: '',
        };

        const newCategory = await api.category.create(formData);
        if (!newCategory) {
            messageBox.show({
                title: 'Thông báo',
                icon: 'sms_failed',
                message: 'Tạo category thất bại !',
                buttons: [{ label: 'Ok' }],
            });
            return;
        }

        setIdCategoryNewest(newCategory._id);
        setCategories((preState) => {
            if (!preState) return undefined;
            return [...preState, newCategory];
        });
    }, [messageBox]);

    const categoryItemElmnts = React.useMemo(() => {
        if (!categories || categories.length === 0) return [];

        const makeCategoryTree = (
            parentId: string | undefined = undefined,
            listAdded: Set<string> = new Set(),
        ): ICategoryTreeItem[] => {
            const items = categories.filter((child) => {
                if (child.parentId !== parentId) return false;
                if (listAdded.has(child._id)) return false;
                return true;
            });

            items.sort((a, b) => {
                if (a.index > b.index) return 1;
                if (a.index < b.index) return -1;

                if (a.createdAt > b.createdAt) return -1;
                if (a.createdAt < b.createdAt) return 1;

                return 0;
            });

            return items.map((child) => {
                listAdded.add(child._id);
                return {
                    ...child,
                    children: makeCategoryTree(child._id, listAdded),
                };
            });
        };

        const listAdded = new Set<string>();
        const categoryTreeObject = makeCategoryTree(undefined, listAdded);

        // add error child. 'error child' is all child which have parentId is _id of undefined child;
        categories.forEach((child) => {
            if (listAdded.has(child._id)) return;

            categoryTreeObject.push({
                ...child,
                children: [],
                isError: true,
            });
            listAdded.add(child._id);
        });

        return categoryTreeObject.map((child) => {
            return <CategoryItem key={child._id} data={child} />;
        });
    }, [categories]);

    React.useEffect(() => {
        loadCategories();
    }, []);

    return (
        <CategoryTreeContext.Provider
            value={{
                loadCategories,
                idCategoryNewest,
                setIdCategoryNewest,
                setCategories,
            }}
        >
            <div className={styles['category-tree']}>
                <div className={styles['btn-add']}>
                    <button onClick={handlerBtnAddClick}>Thêm danh mục mới</button>
                </div>
                {categoryItemElmnts}
            </div>
        </CategoryTreeContext.Provider>
    );
}
