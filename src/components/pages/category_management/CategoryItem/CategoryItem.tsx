import * as React from 'react';
import api from '../../../../api/api';
import { useOpenImagesDialogAsync } from '../../../../hooks/useOpenImagesDialog';
import useMessageBox from '../../../../hooks/userMessageBox';
import LapisCircularButton from '../../../../share/components/LapisCircularButton';
import make from '../../../../share/functions/make';
import makeClassName from '../../../../share/functions/make/className';
import ICategoryFormData from '../../../../share_types/form_data/admin/ICategoryFormData';
import { CategoryTreeContext } from '../CategoryTree/CategoryTree';
import ICategoryTreeItem from '../types/ICategoryTreeItem';

import styles from './CategoryItem.module.scss';

export interface ICategoryItemProps {
    data: ICategoryTreeItem;
}

export default function CategoryItem(props: ICategoryItemProps) {
    const data = props.data;
    const children = data.children;
    const messageBox = useMessageBox();
    const openImageDialog = useOpenImagesDialogAsync();

    const { idCategoryNewest, setIdCategoryNewest, setCategories, loadCategories } =
        React.useContext(CategoryTreeContext);

    const [inputDisable, setInputDisable] = React.useState<boolean>(true);
    const [inputValue, setInputValue] = React.useState<string>(data.title);
    const [inputIndex, setInputIndex] = React.useState<string>(String(data.index));
    const [displayChildren, setDisplayChildren] = React.useState<boolean>(false);

    const inputRef = React.useRef<HTMLInputElement>(null);

    const makeFromData = React.useCallback(() => {
        const formData: ICategoryFormData = {
            index: data.index,
            title: data.title,
            parentId: data.parentId,
            image: data.image,
        };
        return formData;
    }, [data.index, data.title, data.parentId, data.image]);

    const handlerImageClick = React.useCallback(async () => {
        const result = await openImageDialog();
        if (result.selectedImages.length === 0) return;

        const formData = makeFromData();
        formData.image = result.selectedImages[0];

        const newCategory = await api.category.update(data._id, formData);
        if (!newCategory) {
            messageBox.show({
                title: 'Thông báo',
                icon: 'sms_failed',
                message: 'Đổi icon thất bại !',
                buttons: [{ label: 'Ok' }],
            });
            return;
        }

        setCategories((preState) => {
            if (!preState) return undefined;

            return preState.map((item) => {
                if (item._id !== data._id) return item;
                return newCategory;
            });
        });
    }, [data._id, makeFromData, openImageDialog, messageBox, setCategories]);

    const handlerInputChange = React.useCallback((e: React.FormEvent<HTMLInputElement>) => {
        setInputValue(e.currentTarget.value);
    }, []);

    // rename
    const handlerInputBlur = React.useCallback(
        async (e: React.FormEvent<HTMLInputElement>) => {
            setInputDisable(true);

            const newTitle = e.currentTarget.value;
            if (newTitle === data.title) return;

            const formData = makeFromData();
            formData.title = newTitle;

            const newCategory = await api.category.update(data._id, formData);
            if (!newCategory) {
                messageBox.show({
                    title: 'Thông báo',
                    icon: 'sms_failed',
                    message: 'Đổi tên thất bại !',
                    buttons: [{ label: 'Ok' }],
                });
                setInputValue(data.title);
                return;
            }

            setCategories((preState) => {
                if (!preState) return undefined;

                return preState.map((item) => {
                    if (item._id !== data._id) return item;
                    return newCategory;
                });
            });
        },
        [data._id, data.title, makeFromData, messageBox, setCategories],
    );

    const handlerInputKeyup = React.useCallback(
        (e: React.KeyboardEvent<HTMLInputElement>) => {
            if (e.key === 'Enter') {
                handlerInputBlur(e);
            }
        },
        [handlerInputBlur],
    );

    // btn handler

    const handlerBtnRenameClick = React.useCallback(() => {
        // alert('ok');
        setInputDisable(false);
    }, []);

    // add
    const handlerBtnAddClick = React.useCallback(async () => {
        const formData: ICategoryFormData = {
            index: 0,
            title: 'Danh mục mới',
            parentId: data._id,
            image: '',
        };

        const newCategory = await api.category.create(formData);
        if (!newCategory) {
            messageBox.show({
                title: 'Thông báo',
                icon: 'sms_failed',
                message: 'Tạo category thất bại !',
                buttons: [{ label: 'Ok' }],
            });
            return;
        }

        setIdCategoryNewest(newCategory._id);
        setDisplayChildren(true);
        setCategories((preState) => {
            if (!preState) return undefined;
            return [...preState, newCategory];
        });
    }, [data._id, messageBox, setIdCategoryNewest, setCategories]);

    // remove
    const handlerBtnRemoveClick = React.useCallback(() => {
        const handlerRemove = async (removeType: 'one' | 'all') => {
            const categoryRemoved = await api.category.remove(data._id, removeType);

            // case failure
            if (!categoryRemoved) {
                messageBox.show({
                    title: 'Thông báo',
                    icon: 'sms_failed',
                    message: `Xóa ${data.title} thất bại`,
                    buttons: [
                        {
                            label: 'Ok',
                        },
                        {
                            label: 'Thử lại',
                            callback: async () => {
                                handlerRemove(removeType);
                            },
                        },
                    ],
                });
                return;
            }

            // case successfully

            loadCategories();

            // setCategories((preState)=>{
            //     if(!preState) return undefined;

            //     const newState = preState.filter(item => item._id !== data._id);
            //     return newState;
            // });
        };

        messageBox.show({
            title: 'Thông báo',
            icon: 'question_mark',
            message: `Bạn có muốn xóa ${data.title} và tất cả các mục con ?`,
            buttons: [
                {
                    label: 'Chỉ xóa mục này',
                    callback: async () => {
                        handlerRemove('one');
                    },
                },
                {
                    label: 'Mục này và các mục con',
                    callback: async () => {
                        handlerRemove('all');
                    },
                },
                {
                    label: 'Cancel',
                },
            ],
        });
    }, [data.title, data._id, messageBox, loadCategories]);

    const handlerInputIndexChange = React.useCallback((e: React.FormEvent<HTMLInputElement>) => {
        // let v = parseInt(e.currentTarget.value);
        // if(!isFinite(v)){
        //     v = 0;
        // }

        setInputIndex(e.currentTarget.value);
    }, []);

    const handlerInputIndexBlur = React.useCallback(
        async (e: React.FormEvent<HTMLInputElement>) => {
            let newIndex = parseInt(e.currentTarget.value);
            if (!isFinite(newIndex)) {
                newIndex = 0;
            }

            if (data.index === newIndex) return;

            const formData: ICategoryFormData = {
                index: newIndex,
                title: data.title,
                parentId: data.parentId,
                image: data.image,
            };

            const newCategory = await api.category.update(data._id, formData);
            if (!newCategory) {
                messageBox.show({
                    title: 'Thông báo',
                    icon: 'sms_failed',
                    message: 'Thay đổi thứ tự ưu tiên thất bại !',
                    buttons: [{ label: 'Ok' }],
                });
                setInputValue(data.title);
                return;
            }

            setCategories((preState) => {
                if (!preState) return undefined;

                const newState = preState.filter((item) => item._id !== data._id);
                newState.push(newCategory);
                return newState;
            });
        },
        [data._id, data.index, data.title, data.parentId, data.image, setCategories, messageBox],
    );

    React.useEffect(() => {
        if (inputDisable) return;
        if (!inputRef.current) return;

        inputRef.current?.focus();
        inputRef.current.setSelectionRange(0, inputRef.current.value.length);
    }, [inputDisable]);

    const itemsElmnts = React.useMemo(() => {
        if (!children || children.length === 0) return null;
        return children.map((child) => {
            return (
                <li key={child._id}>
                    <CategoryItem data={child} />
                </li>
            );
        });
    }, [children]);

    React.useEffect(() => {
        if (idCategoryNewest !== data._id) return;
        setInputDisable(false);
    }, [idCategoryNewest, data._id]);

    return (
        <div className={make.className([styles['category-item']])}>
            <div className={styles['wrap']}>
                <div className={makeClassName([data.isError && styles['error']])}>
                    <div
                        className={styles['image']}
                        style={{ backgroundImage: `url('${make.imageAddress(data.image, 'thumbnail')}')` }}
                        onClick={handlerImageClick}
                    ></div>

                    <input
                        readOnly={inputDisable}
                        ref={inputRef}
                        type='text'
                        value={inputValue}
                        onKeyUp={handlerInputKeyup}
                        onChange={inputDisable ? undefined : handlerInputChange}
                        onBlur={handlerInputBlur}
                        onDoubleClick={handlerBtnRenameClick}
                    />
                    <div className={styles['buttons']}>
                        <LapisCircularButton
                            className={make.className(['btn', 'rename'], styles)}
                            icon={'edit'}
                            tooltip='Đổi tên'
                            onClick={handlerBtnRenameClick}
                        />
                        <LapisCircularButton
                            className={make.className(['btn', 'add'], styles)}
                            icon={'add'}
                            tooltip='Thêm danh mục con'
                            onClick={handlerBtnAddClick}
                        />
                        <LapisCircularButton
                            className={make.className(['btn', 'remove'], styles)}
                            icon={'delete'}
                            tooltip='Xóa bỏ'
                            onClick={handlerBtnRemoveClick}
                        />

                        <input
                            className={make.className(['input-index'], styles)}
                            type='number'
                            value={inputIndex}
                            onChange={handlerInputIndexChange}
                            onBlur={handlerInputIndexBlur}
                        />
                    </div>

                    <div
                        className={make.className([
                            styles['icon-dropdown'],
                            (children?.length || 0) > 0 && styles['display'],
                            displayChildren && styles['display-children'],
                        ])}
                        onClick={() => {
                            setDisplayChildren((v) => !v);
                        }}
                    >
                        arrow_drop_down
                    </div>
                </div>
            </div>

            {itemsElmnts && <ul className={make.className([displayChildren && styles['display']])}>{itemsElmnts}</ul>}
        </div>
    );
}
