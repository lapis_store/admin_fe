import { CategoryScale, Chart } from 'chart.js';
import * as React from 'react';
import { Doughnut } from 'react-chartjs-2';
import { ChartJSOrUndefined } from 'react-chartjs-2/dist/types';
import make from '../../../../share/functions/make';
import EOrderStatus from '../../../../share_types/database/EOrderStatus';
import IOrderResponse from '../../../../share_types/response/admin/order/TListOrderResponse';
import orderStatusOptions, { mapOrderStatus, orderStatusColor } from '../../order/OrderWrap/orderStatusOptions';

import styles from './OrderStatusChart.module.scss';

Chart.register(CategoryScale);

export interface IOrderStatusChartProps {
    data: IOrderResponse[] | undefined;
    className?: string;
    onClick?: (e: EOrderStatus | undefined) => any;
}

const groupDataByOrderStatus = (
    orders?: IOrderResponse[],
): {
    status: Map<number, EOrderStatus>;
    labels: string[];
    data: number[];
    backgroundColor: string[];
} => {
    if (!orders || orders.length === 0) return { labels: [], data: [], backgroundColor: [], status: new Map() };

    const result = orders.reduce((total, order) => {
        const value = total[order.orderStatus] || 0;
        total[order.orderStatus] = value + 1;

        return total;
    }, {} as { [props: string]: number });

    const keys = Object.keys(result);

    return {
        status: keys.reduce((mapStatus, status, i) => {
            mapStatus.set(i, status as EOrderStatus);
            return mapStatus;
        }, new Map<number, EOrderStatus>()),

        labels: keys.map((key) => {
            const label = mapOrderStatus.get(key) || 'undefined';
            return label;
        }),

        data: Object.values(result),

        backgroundColor: Object.keys(result).map((key) => {
            const color = orderStatusColor.get(key as EOrderStatus) || 'blue';
            return color;
        }),
    };
};

export default function OrderStatusChart(props: IOrderStatusChartProps) {
    const { onClick } = props;
    const chartRef = React.useRef<ChartJSOrUndefined<'doughnut', number[], string>>();

    const data = React.useMemo(() => {
        return groupDataByOrderStatus(props.data);
    }, [props.data]);

    const handlerSliceClick = React.useCallback(
        (index: number) => {
            if (onClick) onClick(data.status.get(index));
        },
        [onClick, data],
    );

    return (
        <div className={make.className([styles['order-status-chart'], props.className])}>
            <Doughnut
                ref={chartRef}
                width={1000}
                height={1000}
                data={{
                    labels: data.labels,
                    datasets: [
                        {
                            data: data.data,
                            backgroundColor: data.backgroundColor,
                        },
                    ],
                }}
                options={{
                    maintainAspectRatio: false,
                    color: 'white',
                    // backgroundColor: 'rgba(0,255,255,0.5)',
                    borderColor: 'rgba(0,255,255,0)',
                    plugins: {
                        legend: {
                            display: true,
                            align: 'center',
                            position: 'right',
                        },
                    },
                    responsive: true,
                    onClick(event, elements, chart) {
                        handlerSliceClick(-1);
                        if (elements.length === 0) return;
                        handlerSliceClick(elements[0].index);
                    },
                }}
            />
        </div>
    );
}
