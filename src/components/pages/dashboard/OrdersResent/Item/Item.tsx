import * as React from 'react';
import { Link } from 'react-router-dom';
import EPages from '../../../../../core/types/EPages';
import IOrderResponse from '../../../../../share_types/response/admin/order/TListOrderResponse';
import { mapOrderStatus, orderStatusColor } from '../../../order/OrderWrap/orderStatusOptions';

import styles from './Item.module.scss';

export interface IItemProps {
    data: IOrderResponse;
}

export default function Item(props: IItemProps) {
    return (
        <Link to={`/${EPages.orderManagement}/${props.data?._id}`} className={styles['item']}>
            <div className={styles['name']}>{props.data.customerName}</div>
            <div className={styles['total']}>
                {props.data.total.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' })}
            </div>
            <div
                className={styles['status']}
                style={{
                    backgroundColor: orderStatusColor.get(props.data.orderStatus),
                }}
            >
                {mapOrderStatus.get(props.data.orderStatus)}
            </div>
            <div className={styles['created-at']}>{new Date(props.data.createdAt).toLocaleString('vi-VN')}</div>
        </Link>
    );
}
