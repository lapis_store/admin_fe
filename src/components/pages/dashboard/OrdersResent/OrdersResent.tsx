import * as React from 'react';
import make from '../../../../share/functions/make';
import EOrderStatus from '../../../../share_types/database/EOrderStatus';
import IOrderResponse from '../../../../share_types/response/admin/order/TListOrderResponse';
import Item from './Item';

import styles from './OrdersResent.module.scss';

export interface IOrdersResentProps {
    data?: IOrderResponse[];
    className?: string;
    filterBy?: EOrderStatus;
}

export default function OrdersResent(props: IOrdersResentProps) {
    const data = React.useMemo(() => {
        if (!props.data) return [];

        if (!props.filterBy) return props.data;

        return props.data.filter((item) => {
            return item.orderStatus === props.filterBy;
        });
    }, [props.data, props.filterBy]);

    const itemsElmnts = React.useMemo(() => {
        return data.map((order) => {
            return <Item key={order._id} data={order} />;
        });
    }, [data]);

    return <div className={make.className([styles['orders-recent'], props.className])}>{itemsElmnts}</div>;
}
