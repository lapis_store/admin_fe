import * as React from 'react';
import make from '../../../../share/functions/make';

import styles from './StatisticalCard.module.scss';

export interface IStatisticalCardProps {
    label: string;
    icon: string;
    value: string;
    className?: string;
}

export default function StatisticalCard(props: IStatisticalCardProps) {
    return (
        <div className={make.className([styles['statistical-card'], props.className])}>
            <div className={styles['icon']}>
                <strong>{props.icon}</strong>
            </div>

            <div className={styles['info']}>
                <div className={styles['value']}>{props.value}</div>
                <div className={styles['label']}>{props.label}</div>
            </div>
        </div>
    );
}
