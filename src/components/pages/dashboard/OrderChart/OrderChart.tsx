import * as React from 'react';

import ChartJS from 'chart.js/auto';
import { CategoryScale } from 'chart.js';
import { Bar } from 'react-chartjs-2';
import IOrderResponse from '../../../../share_types/response/admin/order/TListOrderResponse';
import calc from '../../../../share/functions/calc';
import make from '../../../../share/functions/make';

import styles from './OrderChart.module.scss';

ChartJS.register(CategoryScale);

export interface IOrderChartProps {
    data: IOrderResponse[] | undefined;
    startDate?: Date;
    endDate?: Date;
    average?: number;
    className?: string;
}

const makeKeyByDate = (d: Date): string => {
    const day = d.getDay();
    const dd = d.getDate().toString().padStart(2, '0');
    const mm = d.getMonth().toString().padStart(2, '0');

    const strDay = (() => {
        if (day === 0) return 'CN';
        if (day === 1) return 'T2';
        if (day === 2) return 'T3';
        if (day === 3) return 'T4';
        if (day === 4) return 'T5';
        if (day === 5) return 'T6';
        if (day === 6) return 'T7';
    })();

    return `${strDay} ${dd}/${mm}`;
};

const makeMinMaxDate = (options: {
    startDate?: Date;
    endDate?: Date;
    orders: IOrderResponse[];
}): { minStrDate: string; maxStrDate: string } => {
    const { startDate, endDate, orders } = options;

    if (startDate && endDate) {
        return {
            minStrDate: startDate.toJSON(),
            maxStrDate: endDate.toJSON(),
        };
    }

    let minStrDate = orders[0].createdAt;
    let maxStrDate = orders[0].createdAt;

    orders.forEach((order) => {
        if (order.createdAt > maxStrDate) maxStrDate = order.createdAt;
        if (order.createdAt < minStrDate) minStrDate = order.createdAt;
    });

    return {
        minStrDate: startDate?.toJSON() || minStrDate,
        maxStrDate: endDate?.toJSON() || maxStrDate,
    };
};

const groupDataByDate = (
    orders?: IOrderResponse[],
    options?: { startDate?: Date; endDate?: Date },
): {
    labels: string[];
    ordersInADay: number[];
} => {
    if (!orders || orders.length === 0) return { labels: [], ordersInADay: [] };

    const { minStrDate, maxStrDate } = makeMinMaxDate({ ...options, orders });

    let currentStrDate = minStrDate;
    const result: {
        [props in string]: number;
    } = {};

    while (currentStrDate <= maxStrDate) {
        const d = new Date(currentStrDate);
        const key = makeKeyByDate(d);

        result[key] = 0;
        currentStrDate = calc.LapisDate.plus(1, d).toJSON();
    }

    orders.forEach((order) => {
        const d = new Date(order.createdAt);

        const key = makeKeyByDate(d);
        const numberOfOrders = result[key] || 0;

        result[key] = numberOfOrders + 1;
    });

    const labels: string[] = [];
    const ordersInADay: number[] = [];

    for (const key in result) {
        labels.push(key);
        ordersInADay.push(result[key]);
    }

    return {
        labels,
        ordersInADay,
    };
};

export default function OrderChart(props: IOrderChartProps) {
    const data = React.useMemo(() => {
        return groupDataByDate(props.data, { startDate: props.startDate, endDate: props.endDate });
    }, [props.data, props.startDate, props.endDate]);

    return (
        <div className={make.className([styles['order-chart'], props.className])}>
            <Bar
                width={1000}
                height={382}
                data={{
                    labels: data.labels,
                    datasets: [
                        {
                            data: data.ordersInADay,
                            backgroundColor: data.ordersInADay.map((item) => 'rgba(0,255,255,0.3)'),
                            borderRadius: 3,
                            order: 1,
                        },
                        {
                            data: data.ordersInADay.map((item) => props.average || 0),
                            type: 'line' as any,
                            borderColor: 'rgba(255,255,255,0.1)',
                            pointRadius: 0,
                            order: 0,
                        } as any,
                    ],
                }}
                options={{
                    maintainAspectRatio: false,
                    color: 'white',
                    backgroundColor: 'rgba(0,255,255,0)',
                    borderColor: 'aqua',

                    plugins: {
                        legend: {
                            display: false,
                        },
                    },
                    responsive: true,
                }}
            />
        </div>
    );
}
