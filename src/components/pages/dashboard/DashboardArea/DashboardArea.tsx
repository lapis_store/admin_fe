import * as React from 'react';
import api from '../../../../api/api';
import { IBaseStatisticsResponse } from '../../../../api/order/baseStatistics';
import useMessageBox from '../../../../hooks/userMessageBox';
import calc from '../../../../share/functions/calc';
import make from '../../../../share/functions/make';
import EOrderStatus from '../../../../share_types/database/EOrderStatus';
import IOrderResponse from '../../../../share_types/response/admin/order/TListOrderResponse';
import EarnedChart from '../EarnedChart';
import OrderChart from '../OrderChart';
import OrdersResent from '../OrdersResent';
import OrderStatusChart from '../OrderStatusChart';
import StatisticalCard from '../StatisticalCard';

import styles from './DashboardArea.module.scss';

export interface IDashboardAreaProps {}

export default function DashboardArea(props: IDashboardAreaProps) {
    const [orderStatusFilter, setOrderStatusFilter] = React.useState<EOrderStatus | undefined>(undefined);

    const [orders, setOrders] = React.useState<IOrderResponse[]>([]);
    const [baseStatistics, setBaseStatistics] = React.useState<IBaseStatisticsResponse>({
        averageEarnedInDays: 0,
        averageOrdersPlacedInDays: 0,
        totalEarned: 0,
        totalOrdersPlaced: 0,
    });

    const [time, setTime] = React.useState<{
        startAt: Date;
        endAt: Date;
    }>({
        startAt: calc.LapisDate.plus(-6),
        endAt: new Date(),
    });

    const messageBox = React.useRef(useMessageBox());

    const loadOrders = async () => {
        const resData = await api.order.list({ startAt: time.startAt });
        if (!resData) {
            messageBox.current.show({
                title: 'Thông báo',
                icon: 'sms_failed',
                message: 'Không thể lấy data từ server. Hãy kiểm tra kết nối mạng rồi thử lại',
                buttons: [{ label: 'Ok' }],
            });
            return;
        }
        setOrders(resData);
    };

    const loadBaseStatistics = async () => {
        const resData = await api.order.baseStatistics();
        if (!resData) {
            messageBox.current.show({
                title: 'Thông báo',
                icon: 'sms_failed',
                message: 'Không thể lấy data từ server. Hãy kiểm tra kết nối mạng rồi thử lại',
                buttons: [{ label: 'Ok' }],
            });
            return;
        }
        setBaseStatistics(resData);
    };

    React.useEffect(() => {
        loadOrders();
        loadBaseStatistics();
    }, []); // important. Don't add loadOrders and loadBaseStatistics to dependency;

    return (
        <div className={make.className([styles['dashboard-area']])}>
            <section className={styles['statistical']}>
                <StatisticalCard
                    icon='attach_money'
                    label='Tổng số tiền đã kiếm được.'
                    value={baseStatistics.totalEarned.toLocaleString('vi-VN', { currency: 'VND', style: 'currency' })}
                />
                <StatisticalCard
                    icon='attach_money'
                    label='Trung bình số tiền kiếm được trong ngày.'
                    value={baseStatistics.averageEarnedInDays.toLocaleString('vi-VN', {
                        currency: 'VND',
                        style: 'currency',
                    })}
                />
                <StatisticalCard
                    icon='receipt_long'
                    label='Tổng số đơn hàng đã đặt'
                    value={baseStatistics.totalOrdersPlaced.toLocaleString('vi-VN')}
                />
                <StatisticalCard
                    icon='receipt_long'
                    label='Trung bình số đơn hàng đã đặt trong ngày'
                    value={baseStatistics.averageOrdersPlacedInDays.toLocaleString('vi-VN')}
                />
            </section>

            <section className={styles['orders-earned']}>
                <aside>
                    <h2>Số tiền kiếm được</h2>
                    <EarnedChart
                        className={make.className(['chart'], styles)}
                        data={orders}
                        startDate={time.startAt}
                        endDate={time.endAt}
                        average={baseStatistics.averageEarnedInDays}
                    />
                </aside>
                <aside>
                    <h2>Số đơn hàng</h2>
                    <OrderChart
                        className={make.className(['chart'], styles)}
                        data={orders}
                        startDate={time.startAt}
                        endDate={time.endAt}
                        average={baseStatistics.averageOrdersPlacedInDays}
                    />
                </aside>
            </section>

            <section className={styles['orders-recent-container']}>
                <aside className={styles['chart']}>
                    <OrderStatusChart
                        className={styles['order-status-chart']}
                        data={orders}
                        onClick={(e) => {
                            setOrderStatusFilter(e);
                        }}
                    />
                </aside>
                <aside className={styles['orders-recent']}>
                    <h2>Đơn hàng gần đây</h2>
                    <OrdersResent className={styles['orders']} data={orders} filterBy={orderStatusFilter} />
                </aside>
            </section>
        </div>
    );
}
