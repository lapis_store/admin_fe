import * as React from 'react';

import ChartJS from 'chart.js/auto';
import { CategoryScale } from 'chart.js';
import { Line } from 'react-chartjs-2';
import IOrderResponse from '../../../../share_types/response/admin/order/TListOrderResponse';
import calc from '../../../../share/functions/calc';
import make from '../../../../share/functions/make';

import styles from './EarnedChart.module.scss';
import { ChartJSOrUndefined } from 'react-chartjs-2/dist/types';

ChartJS.register(CategoryScale);

export interface IEarnedChartProps {
    data: IOrderResponse[] | undefined;
    startDate?: Date;
    endDate?: Date;
    className?: string;
    average?: number;
}

const makeKeyByDate = (d: Date): string => {
    const day = d.getDay();
    const dd = d.getDate().toString().padStart(2, '0');
    const mm = d.getMonth().toString().padStart(2, '0');

    const strDay = (() => {
        if (day === 0) return 'CN';
        if (day === 1) return 'T2';
        if (day === 2) return 'T3';
        if (day === 3) return 'T4';
        if (day === 4) return 'T5';
        if (day === 5) return 'T6';
        if (day === 6) return 'T7';
    })();

    return `${strDay} ${dd}/${mm}`;
};

const makeMinMaxDate = (options: {
    startDate?: Date;
    endDate?: Date;
    orders: IOrderResponse[];
}): { minStrDate: string; maxStrDate: string } => {
    const { startDate, endDate, orders } = options;

    if (startDate && endDate) {
        return {
            minStrDate: startDate.toJSON(),
            maxStrDate: endDate.toJSON(),
        };
    }

    let minStrDate = orders[0].createdAt;
    let maxStrDate = orders[0].createdAt;

    orders.forEach((order) => {
        if (order.createdAt > maxStrDate) maxStrDate = order.createdAt;
        if (order.createdAt < minStrDate) minStrDate = order.createdAt;
    });

    return {
        minStrDate: startDate?.toJSON() || minStrDate,
        maxStrDate: endDate?.toJSON() || maxStrDate,
    };
};

const groupDataByDate = (
    orders?: IOrderResponse[],
    options?: { startDate?: Date; endDate?: Date },
): {
    labels: string[];
    earnedInADay: number[];
} => {
    if (!orders || orders.length === 0) return { labels: [], earnedInADay: [] };

    const { minStrDate, maxStrDate } = makeMinMaxDate({ ...options, orders });
    // console.log(minStrDate, maxStrDate);

    let currentStrDate = minStrDate;
    const result: {
        [props in string]: number;
    } = {};

    while (currentStrDate <= maxStrDate) {
        const d = new Date(currentStrDate);
        const key = makeKeyByDate(d);

        result[key] = 0;
        currentStrDate = calc.LapisDate.plus(1, d).toJSON();
    }

    orders.forEach((order) => {
        const d = new Date(order.createdAt);

        const key = makeKeyByDate(d);
        const earned = result[key] || 0;
        result[key] = earned + order.total;
    });

    const labels: string[] = [];
    const earnedInADay: number[] = [];

    for (const key in result) {
        labels.push(key);
        earnedInADay.push(result[key]);
    }

    return {
        labels,
        earnedInADay,
    };
};

export default function EarnedChart(props: IEarnedChartProps) {
    const chartRef = React.useRef<ChartJSOrUndefined<'line', number[], string>>();

    const data = React.useMemo(() => {
        return groupDataByDate(props.data, { startDate: props.startDate, endDate: props.endDate });
    }, [props.data, props.startDate, props.endDate]);

    // const handlerClick:React.MouseEventHandler<HTMLCanvasElement> = (e) => {
    //     console.log('Hello');
    //     const slice = chartRef.current?.getElementsAtEventForMode(e as any, 'nearest', {intersect: true}, true);
    //     console.log(slice?.length);
    // }

    return (
        <div className={make.className([styles['earned-chart'], props.className])}>
            <Line
                ref={chartRef}
                width={1000}
                height={382}
                // onClick={handlerClick}
                data={{
                    labels: data.labels,
                    datasets: [
                        {
                            data: data.earnedInADay,
                            fill: true,
                            cubicInterpolationMode: 'monotone',
                            tension: 0.5,
                            backgroundColor: 'rgba(0,255,255,0.1)',
                            pointStyle: 'circle',
                            pointRadius: 5,
                            pointHoverRadius: 15,
                            order: 0,
                        },
                        {
                            data: data.earnedInADay.map((item) => props.average || 0),
                            cubicInterpolationMode: 'monotone',
                            borderColor: 'rgba(255,255,255,0.1)',
                            fill: false,
                            tension: 0.5,
                            order: 1,
                            pointRadius: 0,
                        },
                    ],
                }}
                options={{
                    maintainAspectRatio: false,
                    color: 'white',
                    backgroundColor: 'rgba(0,255,255,1)',
                    borderColor: 'aqua',

                    plugins: {
                        legend: {
                            display: false,
                        },
                    },
                    responsive: true,
                    // onClick: (event, elements, chart) => {
                    //     console.log('ok');
                    // },
                }}
            />
        </div>
    );
}
