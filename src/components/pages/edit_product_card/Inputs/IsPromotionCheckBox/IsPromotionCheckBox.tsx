import * as React from 'react';
import LapisCheckBox from '../../../../../share/components/LapisCheckBox/LapisCheckBox';
import make from '../../../../../share/functions/make';
import storage from '../../../../../storage';

import styles from './IsPromotionCheckBox.module.scss';

export interface IIsPromotionCheckBoxProps {
    className?: string;
}

export default function IsPromotionCheckBox(props: IIsPromotionCheckBoxProps) {
    const { store, actions, dispatchStore } = storage.editProduct.useEditProduct();

    const handlerChange = (v: boolean) => {
        dispatchStore(actions.isPromotionPrice(v));
    };

    return (
        <div className={make.className([styles['is-promotion'], props.className])}>
            <label>Áp dụng khuyến mãi</label>
            <LapisCheckBox checked={store.response.isPromotionalPrice} onChange={handlerChange} />
        </div>
    );
}
