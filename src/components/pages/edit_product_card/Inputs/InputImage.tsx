import * as React from 'react';
import Input from './Input';

type TProps = {};

type TState = {};

export default class InputImage extends Input<TProps, TState> {
    public constructor(props: TProps) {
        super(props, {} as TState);

        this.title = 'Image';

        this.isUseOpenImagesDialog = true;
        this.displayButton = true;
    }

    protected handlerButtonInInputClick = async () => {
        if (!this.openImagesDialogRef.current) return;
        if (!this.inputRef.current) return;

        const dialogResult = await this.openImagesDialogRef.current.asyncOpen();

        if (dialogResult.selectedImages.length === 0) return;
        this.inputRef.current.value = dialogResult.selectedImages[0];
    };

    protected updateFormData: (v: string) => void = (v) => {
        this.context.dispatchStore(this.context.actions.image(v));
    };

    protected getInitValue: () => string = () => {
        return this.context.store.response.image || '';
    };
}
