import * as React from 'react';
import ICheckResult from '../../../../core/types/ICheckResult';
import CheckProductFormData from '../../../../functions/CheckProductFormData';
import Input from './Input';

type TProps = {};

type TState = {};

export default class InputPrice extends Input<TProps, TState> {
    public constructor(props: TProps) {
        super(props, {} as TState);

        this.title = 'Giá sản phẩm';
        this.inputType = 'number';
        this.placeholder = '0';
    }

    protected validator: () => Promise<ICheckResult> = () => {
        const v = this.context.store.formData.price;
        return CheckProductFormData.price(v);
    };

    protected validateValueEffectDependence: () => any[] = () => {
        return [this.context.store.formData.price];
    };

    protected updateFormData: (v: string) => void = (v) => {
        let n = parseInt(v);
        if (!isFinite(n)) n = 0;

        this.context.dispatchStore(this.context.actions.price(n));
    };

    protected getInitValue: () => string = () => {
        return this.context.store.response.price?.toString() || '';
    };
}
