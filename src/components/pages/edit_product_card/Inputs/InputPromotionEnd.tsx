import * as React from 'react';
import ICheckResult from '../../../../core/types/ICheckResult';
import CheckProductFormData from '../../../../functions/CheckProductFormData';
import converts from '../../../../share/functions/converts';
import Input from './Input';

type TProps = {};

type TState = {};

export default class InputPromotionEnd extends Input<TProps, TState> {
    public constructor(props: TProps) {
        super(props, {} as TState);

        this.title = 'Khuyến mãi kết thúc lúc';
        this.inputType = 'datetime-local';
    }

    protected validator: () => Promise<ICheckResult> = () => {
        const startAt = this.context.store.formData.promotionStartAt;
        const endAt = this.context.store.formData.promotionEndAt;
        return CheckProductFormData.promotionEndAt(endAt, startAt);
    };

    protected validateValueEffectDependence: () => any[] = () => {
        return [this.context.store.formData.promotionStartAt, this.context.store.formData.promotionEndAt];
    };

    protected updateFormData: (v: string) => void = (v) => {
        const vProcessed = converts.inputDatetimeLocalValueToDate(v);
        this.context.dispatchStore(this.context.actions.promotionEndAt(vProcessed ? vProcessed.toJSON() : 'undefined'));
    };

    protected getInitValue: () => string = () => {
        if (!this.context.store.response.promotionEndAt) return '';
        return converts.dateToInputDatetimeLocalValue(new Date(this.context.store.response.promotionEndAt));
    };
}
