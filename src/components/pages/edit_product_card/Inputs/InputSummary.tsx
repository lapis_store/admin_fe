import * as React from 'react';
import CheckProductFormData from '../../../../functions/CheckProductFormData';
import Input from './Input';

type TProps = {};

type TState = {};

export default class InputSummary extends Input<TProps, TState> {
    public constructor(props: TProps) {
        super(props, {} as TState);

        this.title = 'Tóm lược';
        this.multipleLine = true;
    }

    protected updateFormData: (v: string) => void = (v) => {
        this.context.dispatchStore(this.context.actions.summary(v));
    };

    protected getInitValue: () => string = () => {
        return this.context.store.response.summary || '';
    };
}
