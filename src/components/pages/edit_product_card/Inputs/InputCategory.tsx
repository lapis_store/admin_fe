import * as React from 'react';
import api from '../../../../api/api';
import ICheckResult from '../../../../core/types/ICheckResult';
import CheckProductFormData from '../../../../functions/CheckProductFormData';
import LapisSelection from '../../../../share/components/LapisSelection';
import make from '../../../../share/functions/make';
import { IOption } from '../../../../share/types/IOption';
import Input from './Input';

type Props = {};

type State = {
    selectedValue: string | undefined;
    options: IOption[];
};

export default class InputCategory extends Input<Props, State> {
    constructor(props: Props) {
        super(props, {
            selectedValue: undefined,
            options: [],
        });
    }

    private loadOptions = async () => {
        const categories = await api.category.list();
        if (!categories) {
            return;
        }

        const options: IOption[] = await Promise.all(
            categories.map(async (item) => ({
                label: await make.fullCategoryPath(item._id, categories),
                value: item._id,
            })),
        );

        options.sort((a, b) => {
            if (a.label > b.label) return 1;
            if (a.label < b.label) return -1;
            return 0;
        });

        this.setState((preState) => ({
            ...preState,
            options,
        }));
    };

    private handlerSelectionChange = (v: IOption) => {
        this.setState((preState) => ({
            ...preState,
            selectedValue: v.value,
        }));
        this.updateFormData(v.value);
    };

    protected validator: () => Promise<ICheckResult> = () => {
        const v = this.context.store.formData.categoryId;
        return CheckProductFormData.categoryId(v);
    };

    protected validateValueEffectDependence: () => any[] = () => {
        return [this.context.store.formData.categoryId];
    };

    protected updateFormData = (v: string) => {
        this.context.dispatchStore(this.context.actions.categoryId(v));
    };

    protected updateInitValue: () => void = () => {
        this.setState((preState) => ({
            ...preState,
            selectedValue: this.context.store.response.categoryId,
        }));
    };

    public componentDidMount() {
        this.loadOptions();
        super.componentDidMount();
    }

    public render() {
        return (
            <LapisSelection
                className={make.className([this.styles['input'], this.props.className])}
                title='Danh mục'
                displayIcon={this.state.displayIcon}
                helperIcon={this.state.helperIcon}
                helperIconColor={this.state.helperIconColor}
                helperText={this.state.helperText}
                options={this.state.options}
                selectedValue={this.state.selectedValue}
                onChange={this.handlerSelectionChange}
            />
        );
    }
}
