import * as React from 'react';
import ICheckResult from '../../../../core/types/ICheckResult';
import CheckProductFormData from '../../../../functions/CheckProductFormData';
import Input from './Input';

type TProps = {};

type TState = {};

export default class InputSlug extends Input<TProps, TState> {
    public constructor(props: TProps) {
        super(props, {} as TState);

        this.title = 'Slug';
    }

    protected validator: () => Promise<ICheckResult> = () => {
        const v = this.context.store.formData.slug;
        const _id = this.context.store.response._id;
        return CheckProductFormData.slug(v, _id);
    };

    protected validateValueEffectDependence: () => any[] = () => {
        return [this.context.store.formData.slug];
    };

    protected updateFormData: (v: string) => void = (v) => {
        this.context.dispatchStore(this.context.actions.slug(v));
    };

    protected getInitValue: () => string = () => {
        return this.context.store.response.slug || '';
    };
}
