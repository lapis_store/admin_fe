import * as React from 'react';
import ICheckResult from '../../../../core/types/ICheckResult';
import CheckProductFormData from '../../../../functions/CheckProductFormData';
import Input from './Input';

type TProps = {};

type TState = {};

export default class InputTitle extends Input<TProps, TState> {
    public constructor(props: TProps) {
        super(props, {} as TState);

        this.title = 'Tên sản phẩm';
    }
    protected validator: () => Promise<ICheckResult> = () => {
        const v = this.context.store.formData.title;
        return CheckProductFormData.title(v);
    };

    protected validateValueEffectDependence: () => any[] = () => {
        return [this.context.store.formData.title];
    };

    protected updateFormData: (v: string) => void = (v) => {
        this.context.dispatchStore(this.context.actions.title(v));
    };

    protected getInitValue: () => string = () => {
        return this.context.store.response.title || '';
    };
}
