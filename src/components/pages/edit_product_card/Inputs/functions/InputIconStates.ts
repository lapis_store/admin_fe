import { EValidationIcon, EValidationIconColor } from '../../../../../core/types/EValidation';
import { ECheckResultStatus } from '../../../../../core/types/ICheckResult';
import IInputIconState from '../../../../../core/types/IInputIconState';

export class InputIconState {
    public static fail(): IInputIconState {
        return {
            display: true,
            icon: EValidationIcon.noValidate,
            iconColor: EValidationIconColor.noValidate,
        };
    }

    public static warning(): IInputIconState {
        return {
            display: true,
            icon: EValidationIcon.warning,
            iconColor: EValidationIconColor.warning,
        };
    }

    public static info(): IInputIconState {
        return {
            display: true,
            icon: EValidationIcon.info,
            iconColor: EValidationIconColor.info,
        };
    }

    public static pass(): IInputIconState {
        return {
            display: true,
            icon: EValidationIcon.validate,
            iconColor: EValidationIconColor.validate,
        };
    }

    public static makeIconStateFromResultStatus(status: ECheckResultStatus): IInputIconState {
        switch (status) {
            case ECheckResultStatus.fail: {
                return this.fail();
            }
            case ECheckResultStatus.warning: {
                return this.warning();
            }
            case ECheckResultStatus.info: {
                return this.info();
            }
            case ECheckResultStatus.pass: {
                return this.pass();
            }
            default: {
                throw new Error('Invalid status !');
            }
        }
    }
}
