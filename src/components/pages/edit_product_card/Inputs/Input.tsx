import * as React from 'react';
import ICheckResult, { ECheckResultStatus } from '../../../../core/types/ICheckResult';
import { IOpenImagesDialogContainerRef, OpenImagesDialogContainer } from '../../../../hooks/useOpenImagesDialog';
import LapisInput, { ILapisInputRef } from '../../../../share/components/LapisInput';
import make from '../../../../share/functions/make';
import useDispatchStop from '../../../../share/hooks/useDispatchStop';
import storage from '../../../../storage';

import styles from './Input.module.scss';

type TInputProps = {
    className?: string;
};

type TInputState = {
    displayIcon: boolean;
    helperIcon?: string;
    helperIconColor?: string;
    helperText?: string;
};

export default class Input<P, S> extends React.Component<TInputProps & P, TInputState & S> {
    protected styles = styles;

    protected title: string = '';
    protected multipleLine: boolean = false;
    protected inputType: 'text' | 'number' | 'datetime-local' = 'text';
    protected placeholder: string = '';

    protected isUseOpenImagesDialog: boolean = false;

    displayButton: boolean = false;
    buttonIcon: string = 'photo_library';
    buttonIconColor: string = 'aqua';
    buttonTooltip = 'Chọn hình ảnh từ thư viện';

    public static contextType = storage.editProduct.context;
    context!: React.ContextType<typeof storage.editProduct.context>;

    protected inputRef = React.createRef<ILapisInputRef>();
    protected openImagesDialogRef = React.createRef<IOpenImagesDialogContainerRef>();

    protected constructor(props: TInputProps & P, initState: S | (S & TInputState)) {
        super(props);

        this.state = {
            displayIcon: true,
            helperIcon: 'info',
            helperIconColor: 'green',
            helperText: undefined,
            ...initState,
        };
    }

    protected handlerButtonInInputClick = async () => {};

    protected updateFormData = (v: string) => {};

    private handlerTypingStop = useDispatchStop((v: string) => {
        this.updateFormData(v);
    });

    protected readonly handlerInputChange = (v: string) => {
        this.handlerTypingStop(v);
    };

    protected getInitValue = (): string => {
        return '';
    };

    protected updateInitValue = () => {};

    private updateInitValueEffect = make.effect(() => {
        this.updateInitValue();
        // console.log('update init');
        if (!this.inputRef.current) return;
        this.inputRef.current.value = this.getInitValue();
    });

    protected validator = async (): Promise<ICheckResult> => {
        return {
            status: ECheckResultStatus.pass,
            message: '',
        };
    };

    private validateValueEffect = make.effect(async () => {
        const { status, message } = await this.validator();

        switch (status) {
            case ECheckResultStatus.pass: {
                this.setState((preState) => {
                    return {
                        ...preState,
                        displayIcon: false,
                        helperIcon: undefined,
                        helperIconColor: undefined,
                        helperText: message,
                    };
                });
                break;
            }
            case ECheckResultStatus.info: {
                this.setState((preState) => {
                    return {
                        ...preState,
                        displayIcon: true,
                        helperIcon: 'info',
                        helperIconColor: 'blue',
                        helperText: message,
                    };
                });
                break;
            }
            case ECheckResultStatus.warning: {
                this.setState((preState) => {
                    return {
                        ...preState,
                        displayIcon: true,
                        helperIcon: 'warning',
                        helperIconColor: 'rgb(255,255,0)',
                        helperText: message,
                    };
                });
                break;
            }
            case ECheckResultStatus.fail: {
                this.setState((preState) => {
                    return {
                        ...preState,
                        displayIcon: true,
                        helperIcon: 'error',
                        helperIconColor: 'red',
                        helperText: message,
                    };
                });
                break;
            }
        }
    });

    protected validateValueEffectDependence = (): any[] => {
        return [null];
    };

    public componentDidUpdate() {
        this.updateInitValueEffect([this.context.store.response]);
        this.validateValueEffect(this.validateValueEffectDependence());
    }

    public componentDidMount() {}

    private renderOpenImagesDialogContainer = () => {
        if (!this.isUseOpenImagesDialog) return undefined;
        return <OpenImagesDialogContainer ref={this.openImagesDialogRef} />;
    };

    public render() {
        return (
            <>
                {this.renderOpenImagesDialogContainer()}
                <LapisInput
                    className={make.className([styles['input'], this.props.className])}
                    ref={this.inputRef}
                    title={this.title}
                    type={this.inputType}
                    multipleLine={this.multipleLine}
                    placeholder={this.placeholder}
                    displayIcon={this.state.displayIcon}
                    helperIcon={this.state.helperIcon}
                    helperIconColor={this.state.helperIconColor}
                    helperText={this.state.helperText}
                    displayButton={this.displayButton}
                    buttonIcon={this.buttonIcon}
                    buttonIconColor={this.buttonIconColor}
                    buttonTooltip={this.buttonTooltip}
                    onButtonClick={this.handlerButtonInInputClick}
                    onChange={this.handlerInputChange}
                />
            </>
        );
    }
}
