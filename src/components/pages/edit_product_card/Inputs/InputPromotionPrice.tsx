import * as React from 'react';
import ICheckResult from '../../../../core/types/ICheckResult';
import CheckProductFormData from '../../../../functions/CheckProductFormData';
import Input from './Input';

type TProps = {};

type TState = {};

export default class InputPromotionPrice extends Input<TProps, TState> {
    public constructor(props: TProps) {
        super(props, {} as TState);

        this.title = 'Giá khuyến mãi';
        this.inputType = 'number';
        this.placeholder = '0';
    }

    protected validator: () => Promise<ICheckResult> = () => {
        // console.log('validation');
        const v = this.context.store.formData.promotionPrice;
        const price = this.context.store.formData.price;
        return CheckProductFormData.promotionPrice(v, price);
    };

    protected validateValueEffectDependence: () => any[] = () => {
        return [this.context.store.formData.promotionPrice, this.context.store.formData.price];
    };

    protected updateFormData: (v: string) => void = (v) => {
        let n = parseInt(v);
        if (!isFinite(n)) n = 0;

        this.context.dispatchStore(this.context.actions.promotionPrice(n));
    };

    protected getInitValue: () => string = () => {
        return this.context.store.response.promotionPrice?.toString() || '';
    };
}
