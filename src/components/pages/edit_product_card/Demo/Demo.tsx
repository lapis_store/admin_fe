import * as React from 'react';
// import { Link } from 'react-router-dom';
import ProductCard from '../../../../share/components/ProductCard';
import make from '../../../../share/functions/make';
import storage from '../../../../storage';

import styles from './Demo.module.scss';

export interface IDemoProps {
    className?: string;
}

export default function Demo(props: IDemoProps) {
    const { store } = storage.editProduct.useEditProduct();
    const { formData } = store;

    const { title, price, isPromotionalPrice, promotionPrice, summary, image } = formData;

    return (
        <div className={make.className([styles['demo'], props.className])}>
            <ProductCard
                title={title}
                price={price}
                isPromotionalPrice={isPromotionalPrice}
                promotionPrice={promotionPrice}
                summary={summary}
                image={image}
            />
        </div>
    );
}
