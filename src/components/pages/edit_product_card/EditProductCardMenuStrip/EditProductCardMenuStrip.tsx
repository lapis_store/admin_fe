import * as React from 'react';
import { useNavigate } from 'react-router-dom';
import api from '../../../../api/api';
import EPages from '../../../../core/types/EPages';
import ICheckResult from '../../../../core/types/ICheckResult';
import CheckProductFormData from '../../../../functions/CheckProductFormData';
import useMessageBox from '../../../../hooks/userMessageBox';
import LapisMenuStrip, { LapisMenuStripButton } from '../../../../share/components/LapisMenuStrip';
import IProductFormData from '../../../../share_types/form_data/admin/IProductFormData';
import storage from '../../../../storage';

import './EditProductCardMenuStrip.scss';

export interface IEditProductCardControlProps {}

export default function EditProductCardMenuStrip(props: IEditProductCardControlProps) {
    const { store } = storage.editProduct.useEditProduct();
    const { formData, response: productResponse } = store;

    const productId: string | undefined = productResponse._id;

    const navigate = useNavigate();
    const messageBox = useMessageBox();

    const checkAll = React.useCallback(async (formData: IProductFormData, productId?: string) => {
        const { title, slug, price, isPromotionalPrice, promotionPrice, promotionStartAt, promotionEndAt, image } =
            formData;

        const checkArr = [
            CheckProductFormData.title(title),
            CheckProductFormData.slug(slug, productId),
            CheckProductFormData.price(price),
            CheckProductFormData.image(image),
        ];

        if (isPromotionalPrice) {
            checkArr.push(CheckProductFormData.price(promotionPrice));
            checkArr.push(CheckProductFormData.promotionStartAt(promotionStartAt, promotionEndAt));
            checkArr.push(CheckProductFormData.promotionEndAt(promotionEndAt, promotionStartAt));
        }

        const results: ICheckResult[] = await Promise.all(checkArr);

        for (let i = 0; i < results.length; i++) {
            if (results[i].status < 0) {
                console.log(results[i].message);
                return false;
            }
        }

        // if all results have a status of warning or pass
        return true;
    }, []);

    const handlerButtonSaveClick = React.useCallback(async () => {
        const checkAllResult = await checkAll(formData, productId);

        if (!checkAllResult) {
            messageBox.show({
                title: 'Thông báo',
                icon: 'sms_failed',
                message: 'Bạn cần hoàn thiện một vài mục trước khi lưu',
            });
            return;
        }

        // create
        if (!productId) {
            const productRes = await api.product.create(formData);
            if (!productRes) {
                messageBox.show({
                    title: 'Thông báo',
                    icon: 'sms_failed',
                    message: 'Lưu thất bại. Bạn hãy kiểm tra kết nối mạng và thử lại !',
                });
                return;
            }

            if (!productRes._id) {
                messageBox.show({
                    title: 'Thông báo',
                    icon: 'sms_failed',
                    message: 'Một lỗi không xác định !. Bạn hãy chụp màn hình và liên hệ vói quản trị viên để sửa lỗi',
                });
                return;
            }

            navigate(`/${EPages.editProductDetail}/${productRes._id}`, { replace: true });
            return;
        }

        // update
        const productRes = await api.product.update(productId, formData);
        if (!productRes) {
            messageBox.show({
                title: 'Thông báo',
                icon: 'sms_failed',
                message: 'Lưu thất bại. Bạn hãy kiểm tra kết nối mạng và thử lại !',
            });
            return;
        }

        navigate(`/${EPages.productManagement}`, { replace: true });
    }, [productId, formData, navigate, checkAll, messageBox]);

    const handlerButtonCancelClick = React.useCallback(() => {
        navigate('/product-management', { replace: true });
    }, [navigate]);

    return (
        <LapisMenuStrip className='edit-product-card-menu-strip'>
            <LapisMenuStripButton
                icon='save'
                text='Lưu lại'
                color='var(--lapis-color-1)'
                onClick={handlerButtonSaveClick}
            />
            <LapisMenuStripButton
                icon='cancel'
                text='Huỷ bỏ'
                color='var(--lapis-color-2)'
                onClick={handlerButtonCancelClick}
            />
        </LapisMenuStrip>
    );
}
