import * as React from 'react';
import useOpenImagesDialog, {
    OpenImagesDialogContainer,
    IOpenImagesDialogContainerRef,
} from '../../../../hooks/useOpenImagesDialog';
import make from '../../../../share/functions/make';
import storage from '../../../../storage';
import InputCategory from '../Inputs/InputCategory';
import InputImage from '../Inputs/InputImage';
import InputPrice from '../Inputs/InputPrice';
import InputPromotionEnd from '../Inputs/InputPromotionEnd';
import InputPromotionPrice from '../Inputs/InputPromotionPrice';
import InputPromotionStart from '../Inputs/InputPromotionStart';
import InputSlug from '../Inputs/InputSlug';
import InputSummary from '../Inputs/InputSummary';
import InputTitle from '../Inputs/InputTitle';
import IsPromotionCheckBox from '../Inputs/IsPromotionCheckBox';

import styles from './EditingArea.module.scss';

export interface IEditingAreaProps {}

function EditingArea(props: IEditingAreaProps) {
    const { store } = storage.editProduct.useEditProduct();

    const wrapElmntRef = React.useRef<HTMLDivElement>(null);
    const openImagesDialogRef = React.useRef<IOpenImagesDialogContainerRef>(null);

    // const openImagesDialog = React.useRef(useOpenImagesDialog());

    const openImage = async () => {
        if (!openImagesDialogRef.current) return;
        const e = await openImagesDialogRef.current.asyncOpen();

        console.log('image dialog close');
        console.log('dialogResult', e.dialogResult);
        console.log('selectedImages', e.selectedImages);
    };

    React.useEffect(() => {
        // openImage();
    }, []);

    const handlerTransitionEnd = () => {
        if (!wrapElmntRef.current) return;

        if (store.formData.isPromotionalPrice) {
            wrapElmntRef.current.style.overflow = 'visible';
            return;
        }

        wrapElmntRef.current.style.overflow = 'hidden';
    };

    return (
        <div className={styles['editing-area']}>
            <OpenImagesDialogContainer ref={openImagesDialogRef} />
            <InputTitle className={styles['input']} />
            <InputSlug className={styles['input']} />
            <InputCategory className={styles['input']} />
            <InputImage className={styles['input']} />
            <InputPrice className={styles['input']} />
            <IsPromotionCheckBox className={styles['input']} />
            <div
                className={make.className([styles['wrap'], store.formData.isPromotionalPrice && styles['display']])}
                ref={wrapElmntRef}
                onTransitionEnd={handlerTransitionEnd}
                style={{
                    overflow: store.formData.isPromotionalPrice ? undefined : 'hidden',
                }}
            >
                <InputPromotionPrice className={styles['input']} />
                <InputPromotionStart className={styles['input']} />
                <InputPromotionEnd className={styles['input']} />
            </div>
            <InputSummary className={styles['input']} />
        </div>
    );
}

export default EditingArea;
