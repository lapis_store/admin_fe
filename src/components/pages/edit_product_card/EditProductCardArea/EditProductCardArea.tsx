import * as React from 'react';

import EditingArea from '../EditingArea';
import Demo from '../Demo';
import EditProductCardMenuStrip from '../EditProductCardMenuStrip';

import styles from './EditProductCardArea.module.scss';
import make from '../../../../share/functions/make';

export interface IEditProductCardAreaProps {}

export default function EditProductCardArea(props: IEditProductCardAreaProps) {
    return (
        <div className={make.className([styles['edit-product-card-area']])}>
            <EditProductCardMenuStrip />
            <Demo className={styles['demo']} />
            <EditingArea />
        </div>
    );
}
