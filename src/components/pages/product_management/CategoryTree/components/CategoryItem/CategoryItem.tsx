import * as React from 'react';
import { Link } from 'react-router-dom';
import EPages from '../../../../../../core/types/EPages';
import { ProductManagementContext } from '../../../../../../pages/ProductManagementPage';
import make from '../../../../../../share/functions/make';
import ICategoryTreeItem from '../../types/ICategoryTreeItem';

import styles from './CategoryItem.module.scss';

export interface ICategoryItemProps {
    data: ICategoryTreeItem;
}

export default function CategoryItem(props: ICategoryItemProps) {
    const [isExpand, setIsExpand] = React.useState<boolean>(false);
    const { categoryId } = React.useContext(ProductManagementContext);

    const childrenElmnt = React.useMemo(() => {
        if (!props.data.children || props.data.children.length === 0) return null;

        return props.data.children.map((child) => {
            return (
                <li key={child._id}>
                    <CategoryItem key={child._id} data={child} />
                </li>
            );
        });
    }, [props.data.children]);

    return (
        <div className={styles['category-item']}>
            <div className={styles['wrap']}>
                <div>
                    <div
                        className={styles['image']}
                        style={{ backgroundImage: `url('${make.imageAddress(props.data.image, 'thumbnail')}')` }}
                    />
                    <Link
                        to={`/${EPages.productManagement}/${props.data._id}`}
                        className={make.className(['label', categoryId === props.data._id && 'selected'], styles)}
                    >
                        {props.data.title}
                    </Link>

                    <button
                        className={styles['dropdown']}
                        style={{
                            opacity: (props.data.children?.length || 0) > 0 ? '1' : '0',
                        }}
                        onClick={() => {
                            setIsExpand((preState) => !preState);
                        }}
                    >
                        arrow_drop_down
                    </button>
                </div>
            </div>
            <ul
                style={{
                    display: isExpand ? 'block' : 'none',
                }}
            >
                {childrenElmnt}
            </ul>
        </div>
    );
}
