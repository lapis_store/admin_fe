import * as React from 'react';
import api from '../../../../api/api';
import ICategoryResponse from '../../../../share_types/response/admin/category/ICategoryResponse';
import CategoryItem from './components/CategoryItem';
import ICategoryTreeItem from './types/ICategoryTreeItem';

import styles from './CategoryTree.module.scss';
import { Link } from 'react-router-dom';
import EPages from '../../../../core/types/EPages';

export interface ICategoriesProps {
    onSelected?: (categoryId: string) => any;
}

function CategoryTree(props: ICategoriesProps) {
    const [categories, setCategories] = React.useState<ICategoryResponse[] | undefined>(undefined);

    const loadCategories = async () => {
        const resData = await api.category.list();
        setCategories(resData);
    };

    const categoryItemElmnts = React.useMemo(() => {
        if (!categories || categories.length === 0) return [];

        const makeCategoryTree = (
            parentId: string | undefined = undefined,
            listAdded: Set<string> = new Set(),
        ): ICategoryTreeItem[] => {
            const items = categories.filter((child) => {
                if (child.parentId !== parentId) return false;
                if (listAdded.has(child._id)) return false;
                return true;
            });

            items.sort((a, b) => {
                if (a.index > b.index) return 1;
                if (a.index < b.index) return -1;

                if (a.createdAt > b.createdAt) return -1;
                if (a.createdAt < b.createdAt) return 1;

                return 0;
            });

            return items.map((child) => {
                listAdded.add(child._id);
                return {
                    ...child,
                    children: makeCategoryTree(child._id, listAdded),
                };
            });
        };

        const listAdded = new Set<string>();
        const categoryTreeObject = makeCategoryTree(undefined, listAdded);

        // add error child. 'error child' is all child which have parentId is _id of undefined child;
        categories.forEach((child) => {
            if (listAdded.has(child._id)) return;

            categoryTreeObject.push({
                ...child,
                children: [],
                isError: true,
            });
            listAdded.add(child._id);
        });

        return categoryTreeObject.map((child) => {
            return (
                <li key={child._id}>
                    <CategoryItem key={child._id} data={child} />
                </li>
            );
        });
    }, [categories]);

    React.useEffect(() => {
        loadCategories();
    }, []);

    return (
        <div className={styles['category-tree']}>
            <ul>{categoryItemElmnts}</ul>
        </div>
    );
}

export default React.memo(CategoryTree);
