import ICategoryResponse from '../../../../../share_types/response/admin/category/ICategoryResponse';

interface ICategoryTreeItem extends ICategoryResponse {
    children?: ICategoryTreeItem[];
    isError?: boolean;
}

export default ICategoryTreeItem;
