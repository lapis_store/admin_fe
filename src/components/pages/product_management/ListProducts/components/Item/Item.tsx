import * as React from 'react';
import EPages from '../../../../../../core/types/EPages';
import LapisCircularButton from '../../../../../../share/components/LapisCircularButton';
import make from '../../../../../../share/functions/make';
import IProductResponse from '../../../../../../share_types/response/admin/IProductResponse';

import styles from './Item.module.scss';

export interface IItemProps {
    className?: string;
    data: IProductResponse;
    onRemoveClick?: (productId?: string) => any;
}

function Item(props: IItemProps) {
    const { data } = props;

    const handlerButtonRemoveClick = () => {
        if (props.onRemoveClick) props.onRemoveClick(data._id);
    };

    return (
        <div className={make.className([styles['item'], props.className])}>
            <img src={make.imageAddress(props.data.image)} alt={data.title} />
            <aside>
                <div className={styles['title']}>
                    <a href={make.linkProduct(data.slug)}>{data.title}</a>
                </div>
                <div className={styles['id']}>
                    <span>ID: </span>
                    <span>{data._id}</span>
                </div>
                <div className={styles['buttons']}>
                    <LapisCircularButton
                        className={styles['btn']}
                        type='link'
                        icon='edit'
                        href={`/${EPages.editProductCard}/${data._id}`}
                        tooltip='Chỉnh sửa thẻ sản phẩm'
                    />
                    <LapisCircularButton
                        className={styles['btn']}
                        type='link'
                        icon='note_alt'
                        href={`/${EPages.editProductDetail}/${data._id}`}
                        tooltip='Chỉnh sửa trang sản phẩm'
                    />
                    <LapisCircularButton
                        className={styles['btn']}
                        type='button'
                        icon='delete'
                        tooltip='Xoá'
                        onClick={handlerButtonRemoveClick}
                    />
                </div>
            </aside>
        </div>
    );
}

export default Item;
// export default React.memo(ProductItem);
