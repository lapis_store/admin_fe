import * as React from 'react';
import { TLapisReactElements } from '../../../../core/LapisType';
import make from '../../../../share/functions/make';
import IProductResponse from '../../../../share_types/response/admin/IProductResponse';
import Item from './components/Item';

import styles from './ListProducts.module.scss';

export interface IProductsProps {
    className?: string;
    products: IProductResponse[];
    onRemoveClick?: (productId?: string) => any;
}

function ListProducts(props: IProductsProps) {
    const { products, onRemoveClick } = props;

    const itemsElmnts = React.useMemo((): TLapisReactElements => {
        return products.map((elmnt) => {
            return (
                <li key={elmnt._id}>
                    <Item className={styles['item']} data={elmnt} onRemoveClick={onRemoveClick} />
                </li>
            );
        });
    }, [products, onRemoveClick]);

    return (
        <div className={make.className([styles['list-products'], props.className])}>
            <ul>{itemsElmnts}</ul>
        </div>
    );
}

export default React.memo(ListProducts);
