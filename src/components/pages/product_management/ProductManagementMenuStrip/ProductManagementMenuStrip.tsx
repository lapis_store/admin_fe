import * as React from 'react';
import { useNavigate } from 'react-router-dom';
import EPages from '../../../../core/types/EPages';
import { ProductManagementContext } from '../../../../pages/ProductManagementPage';
import LapisMenuStrip, { LapisMenuStripButton } from '../../../../share/components/LapisMenuStrip';

import './ProductManagementMenuStrip.scss';

export interface IProductManagementMenuStripProps {}

function ProductManagementMenuStrip(props: IProductManagementMenuStripProps) {
    const navigate = useNavigate();
    const { loadProducts, categoryId } = React.useContext(ProductManagementContext);

    const handlerBtnAddNewProduct = React.useCallback(() => {
        navigate(`/${EPages.editProductCard}/new`);
    }, [navigate]);

    const handlerBtnSync = React.useCallback(() => {
        loadProducts(categoryId);
    }, [loadProducts, categoryId]);

    const handlerBtnRemoveSelected = React.useCallback(() => {
        navigate(`/${EPages.productManagement}`);
    }, [navigate]);

    return (
        <LapisMenuStrip className='product-management-menu-strip'>
            <LapisMenuStripButton
                text='Làm mới'
                icon='cloud_sync'
                color='var(--lapis-color-1)'
                onClick={handlerBtnSync}
            />
            <LapisMenuStripButton
                text='Thêm mới'
                icon='add_circle'
                color='var(--lapis-color-1)'
                onClick={handlerBtnAddNewProduct}
            />
            <LapisMenuStripButton
                text='Bỏ lọc theo danh mục'
                icon='playlist_remove'
                color={typeof categoryId !== 'undefined' ? 'var(--lapis-color-1)' : 'gray'}
                onClick={handlerBtnRemoveSelected}
            />
        </LapisMenuStrip>
    );
}

export default React.memo(ProductManagementMenuStrip);
