import * as React from 'react';
import api from '../../../../api/api';
import useMessageBox from '../../../../hooks/userMessageBox';
import LapisInput, { ILapisInputRef } from '../../../../share/components/LapisInput';
import LapisMenuStrip, { LapisMenuStripButton } from '../../../../share/components/LapisMenuStrip';
import IHomeFormData from '../../../../share_types/form_data/admin/IHomeFormData';
import IBanner from '../../../../share_types/others/IBanner';
import IHomeResponse from '../../../../share_types/response/home/IHomeResponse';

import styles from './HomeManagementWrap.module.scss';

export interface IHomeManagementWrapProps {}

const makeBanners = (v: string): IBanner[] => {
    const lines = v
        .split('\n')
        .map((line) => line.trim())
        .filter((line) => line.length > 0);

    const items: string[][] = [];
    let item: string[] = [];

    lines.forEach((line) => {
        if (line === '---') {
            items.push(item);
            item = [];
            return;
        }

        item.push(line);
    });

    if (item.length !== 0) {
        items.push(item);
    }

    return items.map((item) => {
        const [image, alt, url] = item;
        return {
            image: image || '#',
            alt: alt || '',
            url: url || '#',
        };
    });
};

const bannerToString = (banner: IBanner) => {
    const { image, alt, url } = banner;

    const result = [];

    if (typeof image === 'string') result.push(image);
    if (typeof alt === 'string') result.push(alt);
    if (typeof url === 'string') result.push(url);

    return result.join('\n');
};

const bannersToString = (banner: IBanner[]) => {
    return banner
        .map((item) => {
            return bannerToString(item);
        })
        .join('\n\n---\n\n');
};

type TInputType = 'carousel' | 'bannersRightCarousel' | 'bannersBelowCarousel' | 'brandBanner';

export default function HomeManagementWrap(props: IHomeManagementWrapProps) {
    const messageBox = useMessageBox();

    const [inputValue, setInputValue] = React.useState<{
        carousel: string;
        bannersRightCarousel: string;
        bannersBelowCarousel: string;
        brandBanner: string;
    }>({
        carousel: '',
        bannersRightCarousel: '',
        bannersBelowCarousel: '',
        brandBanner: '',
    });

    const inputCarouselRef = React.useRef<ILapisInputRef>(null);
    const inputBannersRightCarouselRef = React.useRef<ILapisInputRef>(null);
    const inputBannersBelowCarouselRef = React.useRef<ILapisInputRef>(null);
    const inputBrandBannerRef = React.useRef<ILapisInputRef>(null);

    const mapDataToInput = (homeData?: IHomeResponse) => {
        if (!homeData) return;

        const carousel = bannersToString(homeData.carousel);
        const bannersRightCarousel = bannersToString(homeData.bannersRightCarousel);
        const bannersBelowCarousel = bannersToString(homeData.bannersBelowCarousel);
        const brandBanner = bannersToString(homeData.brand);

        if (!inputCarouselRef.current) return;
        if (!inputBannersRightCarouselRef.current) return;
        if (!inputBannersBelowCarouselRef.current) return;
        if (!inputBrandBannerRef.current) return;

        inputCarouselRef.current.value = carousel;
        inputBannersRightCarouselRef.current.value = bannersRightCarousel;
        inputBannersBelowCarouselRef.current.value = bannersBelowCarousel;
        inputBrandBannerRef.current.value = brandBanner;
    };

    const loadData = async () => {
        const homeData = await api.home.getData();
        if (!homeData) {
            messageBox.show({
                title: 'Thông báo',
                icon: 'sms_failed',
                message: 'Load data từ server thất bại',
                buttons: [{ label: 'Ok' }],
            });
            return;
        }
        mapDataToInput(homeData);
    };

    const handlerBtnSaveClick = async () => {
        const formData: IHomeFormData = {
            carousel: makeBanners(inputValue.carousel),
            bannersRightCarousel: makeBanners(inputValue.bannersRightCarousel),
            bannersBelowCarousel: makeBanners(inputValue.bannersBelowCarousel),
            brand: makeBanners(inputValue.brandBanner),
        };

        const homeData = await api.home.create(formData);
        if (!homeData) {
            messageBox.show({
                title: 'Thông báo',
                icon: 'sms_failed',
                message: 'Load data từ server thất bại',
                buttons: [{ label: 'Ok' }],
            });
            return;
        }

        messageBox.show({
            title: 'Thông báo',
            icon: 'done',
            message: 'Lưu thành công',
            buttons: [{ label: 'Ok' }],
        });

        mapDataToInput(homeData);
    };

    const handlerInputChange = (inputType: TInputType) => (v: string) => {
        setInputValue((preState) => {
            const newState = { ...preState };
            newState[inputType] = v;
            return newState;
        });
    };

    React.useEffect(() => {
        loadData();
    }, []); // <--- important. don't add loadData to dependency. Because it will create infinite loop.

    return (
        <div className={styles['home-management-page']}>
            <LapisMenuStrip className='menu-strip'>
                <LapisMenuStripButton
                    icon='save'
                    text='Lưu lại'
                    color='var(--lapis-color-1)'
                    onClick={handlerBtnSaveClick}
                />
            </LapisMenuStrip>
            <section>
                <LapisInput
                    ref={inputCarouselRef}
                    className={styles['lapis-input']}
                    title='Carousel'
                    multipleLine={true}
                    numberOfLines={20}
                    onChange={handlerInputChange('carousel')}
                />
            </section>
            <section>
                <LapisInput
                    ref={inputBannersRightCarouselRef}
                    className={styles['lapis-input']}
                    title='Banner bên phải carousel'
                    multipleLine={true}
                    numberOfLines={20}
                    onChange={handlerInputChange('bannersRightCarousel')}
                />
            </section>
            <section>
                <LapisInput
                    ref={inputBannersBelowCarouselRef}
                    className={styles['lapis-input']}
                    title='Banner bên dưới carousel'
                    multipleLine={true}
                    numberOfLines={20}
                    onChange={handlerInputChange('bannersBelowCarousel')}
                />
            </section>
            <section>
                <LapisInput
                    ref={inputBrandBannerRef}
                    className={styles['lapis-input']}
                    title='Banner thương hiệu'
                    multipleLine={true}
                    numberOfLines={20}
                    onChange={handlerInputChange('brandBanner')}
                />
            </section>
        </div>
    );
}
