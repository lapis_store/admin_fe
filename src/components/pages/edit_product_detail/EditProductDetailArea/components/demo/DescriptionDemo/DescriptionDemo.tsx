import * as React from 'react';
import Article from '../../../../../../../share/components/Article';
import make from '../../../../../../../share/functions/make';
import storage from '../../../../../../../storage';

import styles from './DescriptionDemo.module.scss';

export interface IDescriptionDemoProps {
    className?: string;
}

export default function DescriptionDemo(props: IDescriptionDemoProps) {
    const { store } = storage.productDetail.useProductDetail();

    return (
        <div className={make.className([styles['description-demo'], props.className])}>
            <Article data={store.formData.description} />
        </div>
    );
}
