import * as React from 'react';
import ImagesProductSlider from '../../../../../../../share/components/ImagesProductSlider';
import make from '../../../../../../../share/functions/make';
import storage from '../../../../../../../storage';

import styles from './ProductDetailImagesDemo.module.scss';

export interface IProductDetailImagesDemoProps {
    className?: string;
}

export default function ProductDetailImagesDemo(props: IProductDetailImagesDemoProps) {
    const { store } = storage.productDetail.useProductDetail();
    const images = store.formData.images;

    return (
        <div className={make.className([styles['product-detail-image-demo'], props.className])}>
            <ImagesProductSlider images={images} />
        </div>
    );
}
