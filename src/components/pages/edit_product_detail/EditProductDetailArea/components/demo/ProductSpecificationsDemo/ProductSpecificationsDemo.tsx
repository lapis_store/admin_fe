import * as React from 'react';
import ProductSpecifications from '../../../../../../../share/components/ProductSpecifications';
import make from '../../../../../../../share/functions/make';
import storage from '../../../../../../../storage';

import styles from './ProductSpecificationsDemo.module.scss';

export interface IProductSpecificationsDemoProps {
    className?: string;
}

export default function ProductSpecificationsDemo(props: IProductSpecificationsDemoProps) {
    const { store } = storage.productDetail.useProductDetail();
    return (
        <div className={make.className([styles['product-specifications-demo'], props.className])}>
            <ProductSpecifications data={store.formData.specifications} />
        </div>
    );
}
