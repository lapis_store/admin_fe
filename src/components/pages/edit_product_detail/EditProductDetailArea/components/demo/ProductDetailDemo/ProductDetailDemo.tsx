import * as React from 'react';
import ProductDetail from '../../../../../../../share/components/ProductDetail';
import make from '../../../../../../../share/functions/make';
import storage from '../../../../../../../storage';

import styles from './ProductDetailDemo.module.scss';

export interface IProductDetailDemoProps {
    className?: string;
}

export default function ProductDetailDemo(props: IProductDetailDemoProps) {
    const { store } = storage.productDetail.useProductDetail();
    const { title, price, isPromotionalPrice, promotionPrice } = store.productResponse;
    const { promotion, information } = store.formData;
    return (
        <div className={make.className([styles['product-detail-demo'], props.className])}>
            <ProductDetail
                title={title}
                price={price}
                isPromotion={isPromotionalPrice}
                promotionPrice={promotionPrice}
                information={information}
                promotion={promotion}
            />
        </div>
    );
}
