import * as React from 'react';
import Input from './Input';

type TProps = {};

type TState = {};

export default class InputPromotion extends Input<TProps, TState> {
    public constructor(props: TProps) {
        super(props, {} as TState);

        this.title = 'Khuyến mãi';

        this.multipleLine = true;
        this.numberOfLines = 10;

        this.isUseOpenImagesDialog = false;
        this.displayButton = false;
    }

    protected updateFormData: (v: string) => void = (v) => {
        this.context.dispatchStore(this.context.actions.promotion(v));
    };

    protected getInitValue: () => string = () => {
        return this.context.store.response.promotion || '';
    };
}
