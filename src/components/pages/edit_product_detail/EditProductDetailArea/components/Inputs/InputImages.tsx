import * as React from 'react';
import Input from './Input';

type TProps = {};

type TState = {};

export default class InputImages extends Input<TProps, TState> {
    public constructor(props: TProps) {
        super(props, {} as TState);

        this.title = 'Danh sách ảnh sản phẩm';

        this.multipleLine = true;
        this.numberOfLines = 20;

        this.isUseOpenImagesDialog = true;
        this.displayButton = true;
    }

    protected handlerButtonInInputClick = async () => {
        if (!this.openImagesDialogRef.current) return;
        if (!this.inputRef.current) return;

        const dialogResult = await this.openImagesDialogRef.current.asyncOpen();

        if (dialogResult.selectedImages.length === 0) return;
        this.inputRef.current.value = dialogResult.selectedImages.join('\n');
    };

    protected updateFormData: (v: string) => void = (v) => {
        const value = v
            .split('\n')
            .map((item) => item.trim())
            .filter((item) => item.length > 0);
        this.context.dispatchStore(this.context.actions.images(value));
    };

    protected getInitValue: () => string = () => {
        return this.context.store.response.images?.join('\n') || '';
    };
}
