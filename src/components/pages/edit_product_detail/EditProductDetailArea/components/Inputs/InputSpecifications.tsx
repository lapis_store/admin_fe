import * as React from 'react';
import ISpecification from '../../../../../../share_types/others/ISpecification';
import Input from './Input';

type TProps = {};

type TState = {};

export default class InputSpecifications extends Input<TProps, TState> {
    public constructor(props: TProps) {
        super(props, {} as TState);

        this.title = 'Thông số kỹ thuật';

        this.multipleLine = true;
        this.numberOfLines = 15;
    }

    protected updateFormData: (v: string) => void = (v) => {
        const value = v
            .split('\n')
            .map((item) => item.trim())
            .filter((item) => item.length > 0)
            .map((item) => {
                const tmp = item.split(':');
                return {
                    name: tmp[0] || '',
                    value: tmp[1] || '',
                } as ISpecification;
            });
        this.context.dispatchStore(this.context.actions.specifications(value));
    };

    protected getInitValue: () => string = () => {
        const specifications = this.context.store.response.specifications;
        if (!specifications || specifications.length === 0) return '';

        return specifications
            .map((item) => {
                return `${item.name}: ${item.value}`;
            })
            .join('\n');
    };
}
