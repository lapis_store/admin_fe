import * as React from 'react';
import Input from './Input';

type TProps = {};

type TState = {};

export default class InputDescription extends Input<TProps, TState> {
    public constructor(props: TProps) {
        super(props, {} as TState);

        this.title = 'Mô tả sản phẩm';

        this.multipleLine = true;
        this.numberOfLines = 50;

        this.isUseOpenImagesDialog = false;
        this.displayButton = false;
    }

    protected updateFormData: (v: string) => void = (v) => {
        this.context.dispatchStore(this.context.actions.description(v));
    };

    protected getInitValue: () => string = () => {
        return this.context.store.response.description || '';
    };
}
