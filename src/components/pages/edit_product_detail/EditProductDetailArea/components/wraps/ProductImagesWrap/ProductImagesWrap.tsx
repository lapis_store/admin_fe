import * as React from 'react';
import ProductDetailImagesDemo from '../../demo/ProductDetailImagesDemo';
import InputImages from '../../Inputs/InputImages';

import styles from './ProductImagesWrap.module.scss';

export interface IProductImagesWrapProps {}

export default function ProductImagesWrap(props: IProductImagesWrapProps) {
    return (
        <div className={styles['product-images-wrap']}>
            <ProductDetailImagesDemo className={styles['demo']} />
            <div className={styles['inputs']}>
                <InputImages className={styles['input']} />
            </div>
        </div>
    );
}
