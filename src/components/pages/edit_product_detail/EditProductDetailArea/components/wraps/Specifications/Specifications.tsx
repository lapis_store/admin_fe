import * as React from 'react';
import make from '../../../../../../../share/functions/make';
import ProductSpecificationsDemo from '../../demo/ProductSpecificationsDemo';
import InputSpecifications from '../../Inputs/InputSpecifications';

import styles from './Specifications.module.scss';

export interface ISpecificationsProps {
    className?: string;
}

export default function Specifications(props: ISpecificationsProps) {
    return (
        <div className={make.className([styles['specifications'], props.className])}>
            <ProductSpecificationsDemo className={styles['demo']} />
            <div className={styles['inputs']}>
                <InputSpecifications />
            </div>
        </div>
    );
}
