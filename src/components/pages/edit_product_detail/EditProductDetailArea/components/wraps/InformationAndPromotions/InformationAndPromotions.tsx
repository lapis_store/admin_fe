import * as React from 'react';
import make from '../../../../../../../share/functions/make';
import ProductDetailDemo from '../../demo/ProductDetailDemo';
import InputInformation from '../../Inputs/InputInformation';
import InputPromotion from '../../Inputs/InputPromotion';

import styles from './InformationAndPromotions.module.scss';

export interface IInformationAndPromotionsProps {}

export default function InformationAndPromotions(props: IInformationAndPromotionsProps) {
    return (
        <div className={make.className([styles['information-and-promotions']])}>
            <div className={styles['inputs']}>
                <InputInformation className={styles['input']} />
                <InputPromotion className={styles['input']} />
            </div>
            <ProductDetailDemo className={styles['demo']} />
        </div>
    );
}
