import * as React from 'react';
import make from '../../../../../../../share/functions/make';
import DescriptionDemo from '../../demo/DescriptionDemo';
import InputDescription from '../../Inputs/InputDescription';

import styles from './Descriptions.module.scss';

export interface IDescriptionsProps {
    className?: string;
}

export default function Descriptions(props: IDescriptionsProps) {
    return (
        <div className={make.className([styles['description'], props.className])}>
            <div className={styles['inputs']}>
                <InputDescription />
            </div>
            <DescriptionDemo className={styles['demo']} />
        </div>
    );
}
