import * as React from 'react';
import ProductDetailMenuStrip from '../ProductDetailMenuStrip';
import ProductImagesWrap from './components/wraps/ProductImagesWrap';
import InformationAndPromotions from './components/wraps/InformationAndPromotions';
import Specifications from './components/wraps/Specifications';
import Descriptions from './components/wraps/Descriptions';

import styles from './EditProductDetailArea.module.scss';

export interface IEditProductDetailAreaProps {}

export default function EditProductDetailArea(props: IEditProductDetailAreaProps) {
    return (
        <div className={styles['edit-product-detail-area']}>
            <ProductDetailMenuStrip />
            <br />
            <section>
                <h2>Ảnh sản phẩm</h2>
                <ProductImagesWrap />
            </section>
            <section>
                <h2>Thông tin sản phẩm và khuyến mãi</h2>
                <InformationAndPromotions />
            </section>
            <section>
                <h2>Thông số kỹ thuật</h2>
                <Specifications />
            </section>
            <section>
                <h2>Bài viết mô tả sản phẩm</h2>
                <Descriptions />
            </section>
        </div>
    );
}
