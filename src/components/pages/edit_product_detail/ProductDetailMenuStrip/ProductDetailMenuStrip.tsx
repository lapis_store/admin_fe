import * as React from 'react';
import { useNavigate } from 'react-router-dom';
import api from '../../../../api/api';
import useMessageBox from '../../../../hooks/userMessageBox';
import LapisMenuStrip, { LapisMenuStripButton } from '../../../../share/components/LapisMenuStrip';
import IProductDetailResponse from '../../../../share_types/response/admin/IProductDetailResponse';
import useProductDetail from '../../../../storage/productDetail/useProductDetail';

import './ProductDetailMenuStrip.scss';

export interface IProductDetailMenuStripProps {}

export default function ProductDetailMenuStrip(props: IProductDetailMenuStripProps) {
    const { store } = useProductDetail();
    const { formData, productResponse } = store;
    const productId = productResponse._id;

    const navigate = useNavigate();
    const messageBox = useMessageBox();

    const handlerBtnSaveClick = React.useCallback(async () => {
        let res: IProductDetailResponse | undefined = undefined;

        // create
        if (!formData._id) {
            res = await api.productDetail.create({
                ...formData,
                _id: productId,
            });
        } else {
            res = await api.productDetail.update({
                ...formData,
            });
        }

        if (!res) {
            messageBox.show({
                title: 'Thông báo',
                icon: 'sms_failed',
                message: 'Có lỗi trong quá trình lưu. Hãy kiểm tra kết nối internet hoặc đăng nhập lại !',
                buttons: [{ label: 'Ok' }],
            });
            return;
        }

        navigate('/product-management', { replace: false });
    }, [formData, productId, navigate, messageBox]);

    const handlerBtnCancelClick = React.useCallback(() => {
        navigate('/product-management', { replace: false });
    }, [navigate]);

    return (
        <LapisMenuStrip className='product-detail-menu-strip'>
            <LapisMenuStripButton
                icon='save'
                text='Lưu lại'
                color='var(--lapis-color-1)'
                onClick={handlerBtnSaveClick}
            />
            <LapisMenuStripButton
                icon='cancel'
                text='Huỷ bỏ'
                color='var(--lapis-color-2)'
                onClick={handlerBtnCancelClick}
            />
        </LapisMenuStrip>
    );
}
