import * as React from 'react';
import { useNavigate } from 'react-router-dom';
import api from '../../../../api/api';
import EPages from '../../../../core/types/EPages';
import useMessageBox from '../../../../hooks/userMessageBox';
import make from '../../../../share/functions/make';
import EOrderStatus from '../../../../share_types/database/EOrderStatus';
import IOrderResponse from '../../../../share_types/response/admin/order/TListOrderResponse';
import OrderStatusChart from '../../dashboard/OrderStatusChart';
import OrderDetail from './components/OrderDetail';
import Orders from './components/Orders';

import styles from './OrderWrap.module.scss';

export interface IOrderWrapProps {
    idOrderSelected?: string;
}

export const OrderAreaContext = React.createContext<{
    changeStatus: (orderId: string, status: EOrderStatus) => Promise<void>;
}>({} as any);

export default function OrderWrap(props: IOrderWrapProps) {
    const [ordersResponse, setOrdersResponse] = React.useState<IOrderResponse[] | undefined>(undefined);
    const [orderSelected, setOrderSelected] = React.useState<IOrderResponse | undefined>(undefined);
    const [orderStatusFilter, setOrderStatusFilter] = React.useState<EOrderStatus | undefined>();

    const messageBox = useMessageBox();
    const navigate = useNavigate();

    const loadData = async () => {
        const resData = await api.order.list();
        setOrdersResponse(resData);
    };

    const loadOrderSelected = React.useCallback(
        async (id?: string) => {
            if (!id) {
                setOrderSelected(undefined);
                return;
            }

            // find in local
            const _orderSelected = ordersResponse?.find((item) => item._id === id);
            // const _orderSelected = undefined;

            // In case not found. orderSelected will get from server.
            if (!_orderSelected) {
                const orderRes = await api.order.find(id);
                setOrderSelected(orderRes);
                return;
            }

            setOrderSelected(_orderSelected);
        },
        [ordersResponse],
    );

    const changeStatus = React.useCallback(
        async (orderId: string, status: EOrderStatus) => {
            const orderResponse = await api.order.updateStatus(orderId, status);

            if (!orderResponse) {
                messageBox.show({
                    title: 'Thông báo',
                    icon: 'sms_failed',
                    message: `Thay đổi trạng thái cho đơn hàng ${orderId} thất bại`,
                    buttons: [{ label: 'Ok' }],
                });
                return;
            }

            setOrderSelected((_orderSelected) => {
                if (!_orderSelected) return undefined;
                if (_orderSelected._id === orderId) {
                    return orderResponse;
                }
                return _orderSelected;
            });

            setOrdersResponse((preState) => {
                if (!preState) return undefined;

                return preState.map((item) => {
                    if (item._id !== orderId) return item;
                    return orderResponse;
                });
            });
        },
        [messageBox],
    );

    const handlerOrderClick = React.useCallback(
        (id: string) => {
            navigate(`/${EPages.orderManagement}/${id}`);
        },
        [navigate],
    );

    const handlerChartClick = React.useCallback((e?: EOrderStatus) => {
        setOrderStatusFilter(e);
    }, []);

    React.useEffect(() => {
        loadOrderSelected(props.idOrderSelected);
    }, [props.idOrderSelected, loadOrderSelected]);

    React.useEffect(() => {
        loadData();
    }, []);

    return (
        <div className={make.className([styles['order-wrap']])}>
            <OrderAreaContext.Provider value={{ changeStatus }}>
                <aside className={styles['detail']}>
                    <div>
                        {orderSelected !== undefined ? (
                            <OrderDetail value={orderSelected} />
                        ) : (
                            <OrderStatusChart
                                data={ordersResponse}
                                className={styles['chart']}
                                onClick={handlerChartClick}
                            />
                        )}
                    </div>
                </aside>
                <aside className={styles['list']}>
                    <Orders data={ordersResponse} orderStatusFilter={orderStatusFilter} onClick={handlerOrderClick} />
                </aside>
            </OrderAreaContext.Provider>
        </div>
    );
}
