import * as React from 'react';
import LapisBaseSelection, { IOption } from '../../../../../../../../share/components/LapisBaseSelection';
import converts from '../../../../../../../../share/functions/converts';
import make from '../../../../../../../../share/functions/make';
import IOrderResponse from '../../../../../../../../share_types/response/admin/order/TListOrderResponse';
import orderStatusOptions, { orderStatusColor } from '../../../../orderStatusOptions';
import { OrderAreaContext } from '../../../../OrderWrap';

import styles from './Item.module.scss';

export interface IItemProps {
    className?: string;
    data: IOrderResponse;

    onClick?: (id: string) => any;
}

export default function Item(props: IItemProps) {
    const { changeStatus } = React.useContext(OrderAreaContext);

    const handlerBaseSelectionSelected = (v: IOption) => {
        changeStatus(props.data._id, v.value);
    };

    const handlerClick = () => {
        if (props.onClick) props.onClick(props.data._id);
    };

    return (
        <div
            className={make.className([styles['item'], props.className])}
            style={{ borderColor: orderStatusColor.get(props.data.orderStatus) }}
        >
            <div className={styles['info']}>
                <div className={styles['customer-name']} onClick={handlerClick}>
                    {props.data.customerName}
                </div>
                <div className={styles['order-id']}>{props.data._id}</div>
                <div className={styles['address']}>{props.data.address}</div>
                <div className={styles['total']}>
                    <strong>Tổng cộng </strong>
                    <span>{props.data.total?.toLocaleString('vi-VN')}</span>
                </div>
            </div>

            <div className={styles['time']}>
                <div className={styles['update-at']}>
                    <strong>Đã tạo:</strong>
                    <span>{converts.dateToStringLocalDate(props.data.createdAt)}</span>
                </div>
                <div className={styles['created-at']}>
                    <strong>Cập nhật lần cuối:</strong>
                    <span>{converts.dateToStringLocalDate(props.data.updatedAt)}</span>
                </div>
            </div>

            <div className={styles['order-status']}>
                <LapisBaseSelection
                    options={orderStatusOptions}
                    selectedValue={props.data.orderStatus}
                    onSelected={handlerBaseSelectionSelected}
                />
            </div>
        </div>
    );
}
