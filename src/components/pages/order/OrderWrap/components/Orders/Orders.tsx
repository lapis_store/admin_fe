import * as React from 'react';
import make from '../../../../../../share/functions/make';
import EOrderStatus from '../../../../../../share_types/database/EOrderStatus';
import IOrderResponse from '../../../../../../share_types/response/admin/order/TListOrderResponse';
import Item from './components/Item';

import styles from './Orders.module.scss';

export interface IOrderProps {
    className?: string;
    data?: IOrderResponse[];
    orderStatusFilter?: EOrderStatus | undefined;
    onClick?: (id: string) => any;
}

function Orders(props: IOrderProps) {
    const { onClick } = props;
    const data = React.useMemo(() => {
        if (!props.data) return [];

        if (!props.orderStatusFilter) return props.data;

        return props.data.filter((order) => {
            return order.orderStatus === props.orderStatusFilter;
        });
    }, [props.orderStatusFilter, props.data]);

    const itemsElmnts = React.useMemo(() => {
        if (!data) return null;
        return data.map((item) => {
            return <Item key={item._id} data={item} onClick={onClick} />;
        });
    }, [data, onClick]);

    return <div className={make.className([styles['orders'], props.className])}>{itemsElmnts}</div>;
}

export default React.memo(Orders);
