import * as React from 'react';
import { IOrderItemResponse } from '../../../../../../../../share_types/response/admin/order/TListOrderResponse';
import Item from '../Item/Item';

export interface IItemsProps {
    data: IOrderItemResponse[];
}

export default function Items(props: IItemsProps) {
    const itemsElmnt = props.data.map((item) => {
        return <Item key={item.productId} data={item} />;
    });

    return <div>{itemsElmnt}</div>;
}
