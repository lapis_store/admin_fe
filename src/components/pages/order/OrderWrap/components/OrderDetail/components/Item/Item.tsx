import * as React from 'react';
import api from '../../../../../../../../api/api';
import make from '../../../../../../../../share/functions/make';
import { IOrderItemResponse } from '../../../../../../../../share_types/response/admin/order/TListOrderResponse';

import styles from './Item.module.scss';

export interface IItemProps {
    className?: string;
    data: IOrderItemResponse;
}

export default function Item(props: IItemProps) {
    const [imageName, setImageName] = React.useState<string>('');
    const total = (props.data.price * props.data.quantity).toLocaleString('vi-VN');

    const loadImageName = async () => {
        const imageNameRes = await api.product.imageName(props.data.productId);
        if (!imageNameRes) return;
        setImageName(imageNameRes);
    };

    React.useEffect(() => {
        loadImageName();
    }, []);

    return (
        <div className={make.className([styles['item'], props.className])}>
            <img src={make.imageAddress(imageName, 'thumbnail')} alt='' />
            <div className={styles['info']}>
                <div className={styles['product-name']}>{props.data.productName}</div>

                <div className={styles['below-product-name']}>
                    <div className={styles['left']}>
                        <div className={make.className(['product-id', 'item-info'], styles)}>
                            <span className={styles['label']}>Mã</span>
                            <span className={styles['value']}>{props.data.productId}</span>
                        </div>

                        <div className={make.className(['quantity', 'item-info'], styles)}>
                            <span className={styles['label']}>Số lượng</span>
                            <span className={styles['value']}>{props.data.quantity.toLocaleString('vi-VN')}</span>
                        </div>

                        <div className={make.className(['price', 'item-info'], styles)}>
                            <span className={styles['label']}>Đơn giá</span>
                            <span className={styles['value']}>{props.data.price.toLocaleString('vi-VN')}</span>
                        </div>
                    </div>
                    <div className={styles['total']}>{total}</div>
                </div>
            </div>
        </div>
    );
}
