import * as React from 'react';
import { Link } from 'react-router-dom';
import EPages from '../../../../../../core/types/EPages';
import LapisBaseSelection, { IOption } from '../../../../../../share/components/LapisBaseSelection';
import LapisCircularButton from '../../../../../../share/components/LapisCircularButton';
import converts from '../../../../../../share/functions/converts';
import formats from '../../../../../../share/functions/formats';
import make from '../../../../../../share/functions/make';
import IOrderResponse from '../../../../../../share_types/response/admin/order/TListOrderResponse';
import orderStatusOptions from '../../orderStatusOptions';
import { OrderAreaContext } from '../../OrderWrap';
import Items from './components/Items';

import styles from './OrderDetail.module.scss';

export interface IOrderDetailProps {
    value: IOrderResponse;
}

export default function OrderDetail(props: IOrderDetailProps) {
    // const
    const { changeStatus } = React.useContext(OrderAreaContext);

    const handlerOrderStatusChange = (v: IOption) => {
        changeStatus(props.value._id, v.value);
    };

    return (
        <div className={styles['order-detail']}>
            <div className={styles['buttons-wrap']}>
                <LapisCircularButton
                    className={styles['button']}
                    type='link'
                    href={`/${EPages.orderManagement}`}
                    icon={'close'}
                    tooltip='Đóng'
                />
            </div>
            <div className={styles['customer-name']}>
                <strong>Tên khách hàng</strong>
                <span>{props.value.customerName}</span>
            </div>
            <div className={make.className(['order-id', 'item'], styles)}>
                <strong>Mã đơn hàng</strong>
                <span>{props.value._id}</span>
            </div>
            <div className={make.className(['created-at', 'item'], styles)}>
                <strong>Mua ngày</strong>
                <span>{converts.dateToStringLocalDate(props.value?.createdAt)}</span>
            </div>
            <div className={make.className(['phone-number', 'item'], styles)}>
                <strong>Số điện thoại</strong>
                <span>{formats.phoneNumber(props.value.phoneNumber)}</span>
            </div>
            <div className={make.className(['address', 'item'], styles)}>
                <strong>Địa chỉ giao hàng</strong>
                <span>{props.value.address}</span>
            </div>

            <div className={make.className(['order-status', 'item'], styles)}>
                <strong>Trạng thái đơn hàng</strong>
                <LapisBaseSelection
                    options={orderStatusOptions}
                    selectedValue={props.value.orderStatus}
                    onSelected={handlerOrderStatusChange}
                />
            </div>

            <div>
                <Items data={props.value.orderItems} />
            </div>

            <div className={styles['total']}>
                <span className={styles['label']}>Tổng cộng</span>
                <span className={styles['value']}>{props.value.total.toLocaleString('vi-VN')}</span>
            </div>
        </div>
    );
}
