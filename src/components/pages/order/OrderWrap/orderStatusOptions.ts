import { IOption } from '../../../../share/components/LapisBaseSelection';
import EOrderStatus from '../../../../share_types/database/EOrderStatus';

export const orderStatusColor = new Map<EOrderStatus, string>();
orderStatusColor.set(EOrderStatus.unconfirmed, 'rgba(77,0,154,1)');
orderStatusColor.set(EOrderStatus.processing, 'rgba(1,0,154,1)');
orderStatusColor.set(EOrderStatus.inTransit, 'rgba(0,78,153,1)');
orderStatusColor.set(EOrderStatus.completed, 'rgba(0,152,153,1)');
orderStatusColor.set(EOrderStatus.cancelled, 'rgba(255,255,255,0.3)');

const orderStatusOptions: IOption[] = [
    {
        value: EOrderStatus.unconfirmed,
        label: 'Chưa xác nhận',
    },
    {
        value: EOrderStatus.processing,
        label: 'Đang xử lý',
    },
    {
        value: EOrderStatus.inTransit,
        label: 'Đang vận chuyển',
    },
    {
        value: EOrderStatus.completed,
        label: 'Đã giao thành công',
    },
    {
        value: EOrderStatus.cancelled,
        label: 'Đã hủy đơn',
    },
];

export const mapOrderStatus = orderStatusOptions.reduce((total, option) => {
    total.set(option.value, option.label);
    return total;
}, new Map<string, string>());

export default orderStatusOptions;
