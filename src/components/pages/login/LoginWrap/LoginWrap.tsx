import * as React from 'react';
import { useNavigate } from 'react-router-dom';
import api from '../../../../api/api';
import EPages from '../../../../core/types/EPages';
import useMessageBox from '../../../../hooks/userMessageBox';
import LapisInput from '../../../../share/components/LapisInput';
import make from '../../../../share/functions/make';

import styles from './LoginWrap.module.scss';

export interface ILoginWrapProps {}

export default function LoginWrap(props: ILoginWrapProps) {
    const navigate = useNavigate();

    const [showPassword, setShowPassword] = React.useState<boolean>(false);
    const messageBox = useMessageBox();

    const [data, setData] = React.useState<{
        userName?: string;
        password?: string;
    }>({});

    const handlerInputUserNameChange = (v: string) => {
        setData((preState) => ({
            ...preState,
            userName: v,
        }));
    };

    const handlerInputPasswordChange = (v: string) => {
        setData((preState) => ({
            ...preState,
            password: v,
        }));
    };

    const handlerSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        if (!data.userName || !data.password) {
            messageBox.show({
                title: 'Thông báo',
                icon: 'info',
                message: 'Bạn không được để trống tên đăng nhập và mật khẩu',
                buttons: [
                    {
                        label: 'Ok',
                    },
                ],
            });
            return;
        }

        const resData = await api.login.signIn(data.userName, data.password);

        if (!resData) {
            messageBox.show({
                title: 'Thông báo',
                icon: 'error',
                message: 'Có lỗi trong quá trình đăng nhập',
                buttons: [
                    {
                        label: 'Ok',
                    },
                ],
            });
            return;
        }

        if (resData.status !== 'successfully') {
            messageBox.show({
                title: 'Thông báo',
                icon: 'sms_failed',
                message: 'Sai tên đăng nhập hoặc mật khẩu',
                buttons: [
                    {
                        label: 'Ok',
                    },
                ],
            });
            return;
        }

        navigate('/');
    };

    return (
        <div className={make.className([styles['login-wrap']])}>
            <form method='POST' onSubmit={handlerSubmit}>
                {/* <div className={styles['logo']}>Đăng nhập</div> */}
                <div className={styles['inputs']}>
                    <LapisInput
                        className={styles['input']}
                        title='Tên đăng nhập'
                        onChange={handlerInputUserNameChange}
                    />
                    <LapisInput
                        className={styles['input']}
                        type={showPassword ? 'text' : 'password'}
                        title='Mật khẩu'
                        displayButton
                        buttonIcon={showPassword ? 'visibility' : 'visibility_off'}
                        buttonIconColor='aqua'
                        onChange={handlerInputPasswordChange}
                        onButtonClick={() => setShowPassword((v) => !v)}
                    />
                    <div className={styles['buttons']}>
                        <input type='submit' value={'Đăng nhập'} />
                    </div>
                </div>
            </form>
        </div>
    );
}
