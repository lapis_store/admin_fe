import * as React from 'react';
import makeClassName from '../../../share/functions/make/className';

import styles from './Container.module.scss';

export interface IContainerProps {
    className?: string;
}

export default function Container(props: React.PropsWithChildren<IContainerProps>) {
    return <div className={makeClassName([styles['container'], props.className])}>{props.children}</div>;
}
