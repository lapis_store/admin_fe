import * as React from 'react';
import ImageGallery from '../components/ImageGallery';
import ListMessageBox from '../components/page_elements/ListMessageBox';
import LapisLocalStorage from '../core/LapisLocalStorage';
import useMessageBox from '../hooks/userMessageBox';
import Router from '../Router';
import LapisGlobalComponents from '../share/components/LapisGlobalComponents';
import storage from '../storage';
import './App.scss';

function App() {
    const { store } = storage.root.useRootStore();
    const { onMouseUp, onMouseMove } = store;

    const messageBox = React.useRef(useMessageBox());

    const handlerGlobalEventMouseUp = React.useCallback(
        (e: MouseEvent) => {
            if (onMouseUp.length === 0) return;

            onMouseUp.forEach((elmnt) => {
                if (elmnt.handler) elmnt.handler(e);
            });
        },
        [onMouseUp],
    );

    const handlerGlobalEventMouseMove = React.useCallback(
        (e: MouseEvent) => {
            if (onMouseMove.length === 0) return;

            onMouseMove.forEach((elmnt) => {
                if (elmnt.handler) elmnt.handler(e);
            });
        },
        [onMouseMove],
    );

    React.useEffect(() => {
        document.onmouseup = handlerGlobalEventMouseUp;
        document.onmousemove = handlerGlobalEventMouseMove;
    }, [handlerGlobalEventMouseUp, handlerGlobalEventMouseMove]);

    React.useEffect(() => {
        if (!store.page) return;
        if (!LapisLocalStorage.instance.setting.notShowMessageAllowAccess) {
            messageBox.current.show({
                title: 'Lời nhắn',
                icon: 'sms',
                message:
                    'Trang này chỉ là trang giới thiệu. Bạn có quyền thêm/sửa/xóa bất kỳ mục nào trong trang này và các trang khác.',
                buttons: [
                    {
                        label: 'Đóng và không hiện lại',
                        callback: () => {
                            LapisLocalStorage.instance.setting.notShowMessageAllowAccess = true;
                            LapisLocalStorage.instance.saveSetting();
                        },
                    },
                    {
                        label: 'Nhắc lại sau',
                    },
                ],
            });
        }
    }, [store.page]);

    return (
        <div className='app'>
            <LapisGlobalComponents>
                <ImageGallery />
            </LapisGlobalComponents>

            <ListMessageBox />

            <Router />
        </div>
    );
}

export default App;
