import api from '../api/api';
import ICheckResult, { ECheckResultStatus } from '../core/types/ICheckResult';

export default class CheckProductFormData {
    public static async categoryId(v?: string): Promise<ICheckResult> {
        if (!v || v === 'undefined')
            return {
                status: ECheckResultStatus.warning,
                message: 'Bạn không nên để trống danh mục sản phẩm. Khách hàng sẽ khó tìm sản phẩm của bạn hơn',
            };
        return {
            status: ECheckResultStatus.pass,
            message: '',
        };
    }

    public static async slug(v?: string, productId?: string): Promise<ICheckResult> {
        if (!v)
            return {
                status: ECheckResultStatus.fail,
                message: 'Bạn không thể để trống mục này',
            };

        const regexSlug = /(^[a-z0-9][a-z0-9-]*[a-z0-9]$)|(^[a-z0-9]$)/;

        if (!regexSlug.test(v))
            return {
                status: ECheckResultStatus.fail,
                message: `Slug chỉ bao gồm các ký tự a -> z, 0 -> 9 và dấu '-'.
                        Slug không được bắt đầu hay kết thúc bằng '-'.`,
            };

        const checkSlugResponse = await api.product.checkSlug(v, productId);

        if (!checkSlugResponse)
            return {
                status: ECheckResultStatus.fail,
                message: `Không thể kiểm tra slug tại server.
                    Xin vui lòng kiểm tra kết nối internet hoặc đăng nhập lại`,
            };

        if (checkSlugResponse.isExisted)
            return {
                status: ECheckResultStatus.fail,
                message: `Slug này đã tồn tại. Xin vui lòng nhập một slug khác!`,
            };

        return {
            status: ECheckResultStatus.pass,
            message: '',
        };
    }

    public static async image(v?: string): Promise<ICheckResult> {
        if (!v)
            return {
                status: ECheckResultStatus.warning,
                message: 'Ảnh sản phẩm không nên để trống',
            };
        return {
            status: ECheckResultStatus.pass,
            message: '',
        };
    }

    public static async title(v?: string): Promise<ICheckResult> {
        if (!v)
            return {
                status: ECheckResultStatus.warning,
                message: 'Tên sản phẩm không nên để trống',
            };

        return {
            status: ECheckResultStatus.pass,
            message: '',
        };
    }

    public static async price(v?: number): Promise<ICheckResult> {
        // default v = 0
        if (v === undefined)
            return {
                status: ECheckResultStatus.pass,
                message: '',
            };

        if (typeof v !== 'number')
            return {
                status: ECheckResultStatus.fail,
                message: 'Giá sản phẩm bạn nhập vào không phải là số',
            };

        if (!isFinite(v))
            return {
                status: ECheckResultStatus.fail,
                message: 'Giá sản phẩm bạn nhập vào không xác định',
            };

        if (v < 0)
            return {
                status: ECheckResultStatus.fail,
                message: 'Giá sản phẩm bạn nhập vào nhỏ hơn 0',
            };

        return {
            status: ECheckResultStatus.pass,
            message: '',
        };
    }

    public static async promotionPrice(v?: number, price?: number): Promise<ICheckResult> {
        if (!v)
            return {
                status: ECheckResultStatus.pass,
                message: '',
            };

        if (!price)
            return {
                status: ECheckResultStatus.pass,
                message: 'Bạn cần phải nhập giá sản phẩm trước khi nhập giá khuyến mãi',
            };

        if (typeof v !== 'number')
            return {
                status: ECheckResultStatus.fail,
                message: 'Giá khuyến mãi bạn nhập vào không phải là số',
            };

        if (isNaN(v))
            return {
                status: ECheckResultStatus.fail,
                message: 'Giá khuyến mãi bạn nhập vào không xác định',
            };

        if (!isFinite(v))
            return {
                status: ECheckResultStatus.fail,
                message: 'Giá khuyến mãi bạn nhập vào quá lớn',
            };

        if (v < 0)
            return {
                status: ECheckResultStatus.fail,
                message: 'Giá khuyến mãi bạn nhập vào nhỏ hơn 0',
            };

        if (!isFinite(price))
            return {
                status: ECheckResultStatus.pass,
                message: '',
            };

        if (v > price)
            return {
                status: ECheckResultStatus.warning,
                message: 'Giá khuyến mãi bạn nhập vào lớn hơn giá sản phẩm',
            };

        return {
            status: ECheckResultStatus.pass,
            message: '',
        };
    }

    public static async promotionStartAt(startAt?: string, endAt?: string): Promise<ICheckResult> {
        if (!startAt)
            return {
                status: ECheckResultStatus.info,
                message: 'Khuyến mãi sẽ được bắt đầu ngay bây giờ !',
            };

        //
        const currentTime: string = new Date().toJSON();

        if (startAt < currentTime)
            return {
                status: ECheckResultStatus.warning,
                message: `Bạn đang đặt thời gian bắt đầu khuyến mãi nhỏ hơn thời gian
                        hiện tại, vì vậy khuyến mãi sẽ được bắt đầu ngay bây giờ.
                        Bạn có thể để trống mục này nếu muốn khuyến mãi được bắt đầu ngay !`,
            };

        //
        if (!endAt)
            return {
                status: ECheckResultStatus.pass,
                message: '',
            };

        if (startAt > endAt)
            return {
                status: ECheckResultStatus.fail,
                message: 'Thời gian bắt đầu lớn hơn thời gian kết thúc khuyến mãi !',
            };

        return {
            status: ECheckResultStatus.pass,
            message: '',
        };
    }

    public static async promotionEndAt(endAt?: string, startAt?: string): Promise<ICheckResult> {
        if (!endAt)
            return {
                status: ECheckResultStatus.info,
                message: 'Khuyến mãi sẽ không bao giờ kết thúc !',
            };

        //
        const currentTime: string = new Date().toJSON();

        if (endAt < currentTime)
            return {
                status: ECheckResultStatus.warning,
                message: `Thời gian kết thúc nhỏ hơn thời gian hiện tại. Khuyến mãi đã kết thúc !`,
            };

        //
        if (!startAt)
            return {
                status: ECheckResultStatus.pass,
                message: '',
            };

        if (startAt > endAt)
            return {
                status: ECheckResultStatus.fail,
                message: 'Thời gian bắt đầu lớn hơn thời gian kết thúc khuyến mãi !',
            };

        return {
            status: ECheckResultStatus.pass,
            message: '',
        };
    }
}
