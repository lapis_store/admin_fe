import * as React from 'react';
import Header from '../../components/page_elements/Header';
import SideBar from '../../components/page_elements/SideBar';
import make from '../../share/functions/make';

import styles from './DefaultLayout.module.scss';

export interface IDefaultLayoutProps {}

export default function DefaultLayout(props: React.PropsWithChildren<IDefaultLayoutProps>) {
    return (
        <div className={make.className([styles['default-layout']])}>
            <section className={styles['left']}>
                <SideBar />
            </section>

            <section className={styles['right']}>
                <Header />
                <div className={styles['page-container']}>{props.children}</div>
            </section>
        </div>
    );
}
