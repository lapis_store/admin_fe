import * as React from 'react';
import { IImageGalleryClose } from '../components/ImageGallery/ImageGallery';
import storage from '../storage';

function useOpenImagesDialog() {
    const { actions, dispatchStore } = storage.root.useRootStore();

    return (callback: (e: IImageGalleryClose) => any) => {
        dispatchStore(actions.onImageGalleryClose(callback));
        dispatchStore(actions.displayGallery(true));
    };
}

export function useOpenImagesDialogAsync() {
    const { actions, dispatchStore } = storage.root.useRootStore();

    return (): Promise<IImageGalleryClose> => {
        return new Promise((resolve, reject) => {
            dispatchStore(
                actions.onImageGalleryClose((e: IImageGalleryClose) => {
                    resolve(e);
                }),
            );
            dispatchStore(actions.displayGallery(true));
        });
    };
}

export interface IOpenImagesDialogContainerProps {}

export interface IOpenImagesDialogContainerRef {
    open: (callback: (e: IImageGalleryClose) => any) => void;
    asyncOpen: () => Promise<IImageGalleryClose>;
}

function OpenImagesDialogContainerWithoutForwardRef(
    props: IOpenImagesDialogContainerProps,
    ref: React.RefObject<IOpenImagesDialogContainerRef> & any,
) {
    const openImagesDialog = useOpenImagesDialog();
    React.useImperativeHandle(
        ref,
        () =>
            ({
                open: (callback) => {
                    if (callback) {
                        openImagesDialog(callback);
                        return;
                    }

                    return new Promise((resolve, rejects) => {
                        openImagesDialog((e) => {
                            resolve(e);
                        });
                    });
                },
                asyncOpen: () => {
                    return new Promise((resolve, rejects) => {
                        openImagesDialog((e) => {
                            resolve(e);
                        });
                    });
                },
            } as IOpenImagesDialogContainerRef),
    );

    return <></>;
}

const OpenImagesDialogContainer = React.forwardRef<IOpenImagesDialogContainerRef, IOpenImagesDialogContainerProps>(
    OpenImagesDialogContainerWithoutForwardRef,
);

export { OpenImagesDialogContainer };

export default useOpenImagesDialog;
