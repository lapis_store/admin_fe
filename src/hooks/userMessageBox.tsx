import { IMessageBox } from '../share/components/MessageBox';
import storage from '../storage';

function useMessageBox() {
    const { dispatchStore, actions } = storage.root.useRootStore();

    return {
        show: (v: IMessageBox) => {
            dispatchStore(actions.addMessageBox(v));
        },
    };
}

export default useMessageBox;
