import * as React from 'react';
import { Routes, Route } from 'react-router-dom';
import EPages from '../core/types/EPages';
import CategoriesManagementPage from '../pages/CategoriesManagementPage';
import EditProductCardPage from '../pages/EditProductCardPage';

import EditProductDetailPage from '../pages/EditProductDetailPage';
import HomeManagementPage from '../pages/HomeManagementPage';
import DashboardPage from '../pages/DashboardPage';
import Login from '../pages/Login';
import OrderManagementPage from '../pages/OrderManagementPage';
import ProductManagementPage from '../pages/ProductManagementPage';

export interface IRouterProps {}

export default function Router(props: IRouterProps) {
    return (
        <>
            <Routes>
                <Route path={`/${EPages.productManagement}`}>
                    <Route index element={<ProductManagementPage />} />
                    <Route path={`:categoryId`} element={<ProductManagementPage />} />
                </Route>
                <Route path={`/${EPages.editProductCard}/:id`} element={<EditProductCardPage />} />
                <Route path={`/${EPages.editProductDetail}/:id`} element={<EditProductDetailPage />} />
                <Route path={`/${EPages.categoryManagement}/`} element={<CategoriesManagementPage />} />
                <Route path={`/${EPages.orderManagement}`}>
                    <Route index element={<OrderManagementPage />} />
                    <Route path={':orderId'} element={<OrderManagementPage />} />
                </Route>
                <Route path={`/${EPages.homeManagement}`} element={<HomeManagementPage />} />
                <Route path={`/${EPages.login}`} element={<Login />} />
                <Route path='/' element={<DashboardPage />} />
            </Routes>
        </>
    );
}
