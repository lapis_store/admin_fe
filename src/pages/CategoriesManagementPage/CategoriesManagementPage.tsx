import * as React from 'react';
// import CategoriesTree from '../../components/pages/category_management/CategoriesTree';
import CategoryTree from '../../components/pages/category_management/CategoryTree';
import Container from '../../components/reuse/Container';
import EPages from '../../core/types/EPages';
import DefaultLayout from '../../layouts/DefaultLayout';
import BasePage from '../BasePage';

export interface ICategoriesManagementPageProps {}

export interface ICategoriesManagementPageState {}

export default class CategoriesManagementPage extends BasePage<
    ICategoriesManagementPageProps,
    ICategoriesManagementPageState
> {
    public constructor(props: ICategoriesManagementPageProps) {
        super(props, {});

        this.pageName = EPages.categoryManagement;
    }

    public render() {
        return (
            <DefaultLayout>
                <Container>
                    <CategoryTree />
                </Container>
            </DefaultLayout>
        );
    }
}
