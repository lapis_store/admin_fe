import React, { Component } from 'react';
import EPages from '../core/types/EPages';
import make from '../share/functions/make';
import storage from '../storage';

type TProps = {};

type TState = {};

export default class BasePage<P, S> extends Component<TProps & P, TState & S> {
    public static contextType = storage.root.context;
    context!: React.ContextType<typeof storage.root.context>;

    protected pageName: EPages = EPages.default;

    protected constructor(props: TProps & P, initState: TState & S) {
        super(props);
        this.state = {
            ...initState,
        };
    }

    private updateSideBar = () => {
        this.context.dispatchStore(this.context.actions.page(this.pageName));
    };

    public componentDidMount() {
        this.updateSideBar();
    }

    render() {
        return <></>;
    }
}
