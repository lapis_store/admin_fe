import * as React from 'react';
import OrderWrap from '../../components/pages/order/OrderWrap';
import Container from '../../components/reuse/Container';
import EPages from '../../core/types/EPages';
import DefaultLayout from '../../layouts/DefaultLayout';
import make from '../../share/functions/make';
import BasePage from '../BasePage';

export type TOrderManagementPageProps = {
    params: any;
};

type TOrderManagementPageState = {
    idOrderSelected?: string;
};

export default class OrderManagementPage extends BasePage<TOrderManagementPageProps, TOrderManagementPageState> {
    public constructor(props: TOrderManagementPageProps) {
        super(props, {});

        this.pageName = EPages.orderManagement;
    }

    private updateOrderIdEffect = make.effect((orderId: string) => {
        this.setState((preState) => ({
            ...preState,
            idOrderSelected: orderId,
        }));
    });

    public componentDidMount(): void {
        super.componentDidMount();
        this.updateOrderIdEffect([this.props.params.orderId]);
    }

    public componentDidUpdate() {
        // super.componentDidUpdate();
        this.updateOrderIdEffect([this.props.params.orderId]);
    }

    public render() {
        return (
            <DefaultLayout>
                <Container>
                    <OrderWrap idOrderSelected={this.state.idOrderSelected} />
                </Container>
            </DefaultLayout>
        );
    }
}
