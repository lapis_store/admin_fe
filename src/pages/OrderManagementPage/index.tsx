import { useParams } from 'react-router-dom';
import PointerOfOrderManagementPage, { TOrderManagementPageProps } from './OrderManagementPage';

export default function OrderManagementPage(props: Omit<TOrderManagementPageProps, 'params'>) {
    const params = useParams();
    return <PointerOfOrderManagementPage {...props} params={params}></PointerOfOrderManagementPage>;
}
