import * as React from 'react';
import EPages from '../../core/types/EPages';
import DefaultLayout from '../../layouts/DefaultLayout';
import Container from '../../components/reuse/Container';
import BasePage from '../BasePage';
import HomeManagementWrap from '../../components/pages/home_management/HomeManagementWrap';

export interface IHomeManagementPageProps {}

export interface IHomeManagementPageState {}

export default class HomeManagementPage extends BasePage<IHomeManagementPageProps, IHomeManagementPageState> {
    public constructor(props: IHomeManagementPageProps) {
        super(props, {});

        this.pageName = EPages.homeManagement;
    }

    public render() {
        return (
            <DefaultLayout>
                <Container>
                    <HomeManagementWrap />
                </Container>
            </DefaultLayout>
        );
    }
}
