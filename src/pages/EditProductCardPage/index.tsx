import { useParams } from 'react-router-dom';
import InsteadOfEditProductCardPage, { TEditProductCardPageProps } from './EditProductCardPage';

export default function EditProductCardPage(props: Omit<TEditProductCardPageProps, 'params'>) {
    return <InsteadOfEditProductCardPage {...props} params={useParams()} />;
}
