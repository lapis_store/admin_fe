import Container from '../../components/reuse/Container';
import storage from '../../storage';
import EditProductContainer from '../../containers/EditProductContainer';
import EditProductCardArea from '../../components/pages/edit_product_card/EditProductCardArea';

import BasePage from '../BasePage';
import DefaultLayout from '../../layouts/DefaultLayout';

import styles from './EditProductCardPage.module.scss';
import EPages from '../../core/types/EPages';

export type TEditProductCardPageProps = {
    params: any;
};

type TEditProductCardPageState = {};

class EditProductCardPage extends BasePage<TEditProductCardPageProps, TEditProductCardPageState> {
    private id: string;
    constructor(props: TEditProductCardPageProps) {
        super(props, {});

        this.id = props.params.id;
        this.pageName = EPages.editProductCard;
    }

    public render() {
        return (
            <DefaultLayout>
                <Container>
                    <storage.editProduct.Provider>
                        <EditProductContainer id={this.id}>
                            <EditProductCardArea />
                        </EditProductContainer>
                    </storage.editProduct.Provider>
                </Container>
            </DefaultLayout>
        );
    }
}

export default EditProductCardPage;
