import * as React from 'react';
import LoginWrap from '../../components/pages/login/LoginWrap/LoginWrap';
import EPages from '../../core/types/EPages';
import BasePage from '../BasePage';

export interface ILoginProps {}

export interface ILoginState {}
export default class Login extends BasePage<ILoginProps, ILoginState> {
    public constructor(props: ILoginProps) {
        super(props, {});

        this.pageName = EPages.login;
    }

    public render() {
        return <LoginWrap />;
    }
}
