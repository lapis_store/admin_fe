import * as React from 'react';
import DashboardArea from '../../components/pages/dashboard/DashboardArea';
import OrderChart from '../../components/pages/dashboard/OrderChart';
import Container from '../../components/reuse/Container';
import EPages from '../../core/types/EPages';
import DefaultLayout from '../../layouts/DefaultLayout';
import BasePage from '../BasePage';

export interface IDashboardPageProps {}

export interface IDashboardPageState {}

export default class DashboardPage extends BasePage<IDashboardPageProps, IDashboardPageState> {
    public constructor(props: IDashboardPageProps) {
        super(props, {});

        this.pageName = EPages.default;
    }

    public render() {
        return (
            <DefaultLayout>
                <Container>
                    <DashboardArea />
                </Container>
            </DefaultLayout>
        );
    }
}
