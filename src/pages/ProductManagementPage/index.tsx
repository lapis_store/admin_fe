import InsteadOfProductManagementPage, {
    ProductManagementContext,
    TProductManagementPageProps,
} from './ProductManagementPage';
import { useParams } from 'react-router-dom';

export { ProductManagementContext };

export default function ProductManagementPage(props: Omit<TProductManagementPageProps, 'params'>) {
    return <InsteadOfProductManagementPage {...props} params={useParams()} />;
}
