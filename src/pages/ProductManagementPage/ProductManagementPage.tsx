import * as React from 'react';
import api from '../../api/api';
import ListProducts from '../../components/pages/product_management/ListProducts';
import CategoryTree from '../../components/pages/product_management/CategoryTree';
import ProductManagementMenuStrip from '../../components/pages/product_management/ProductManagementMenuStrip';
import Container from '../../components/reuse/Container';
import EPages from '../../core/types/EPages';
import DefaultLayout from '../../layouts/DefaultLayout';
import IProductResponse from '../../share_types/response/admin/IProductResponse';
import BasePage from '../BasePage';

import styles from './ProductManagementPage.module.scss';
import make from '../../share/functions/make';

export type TProductManagementPageProps = {
    params: any;
};

type State = {
    products: IProductResponse[];
    categoryId?: string;
};

export const ProductManagementContext = React.createContext<{
    loadProducts: (categoryId?: string) => Promise<void>;
    categoryId?: string;
}>({} as any);

export default class ProductManagementPage extends BasePage<TProductManagementPageProps, State> {
    constructor(props: TProductManagementPageProps) {
        super(props, {
            products: [],
        });

        this.pageName = EPages.productManagement;
    }

    public loadProducts = async (categoryId?: string) => {
        const resData = await api.product.list(categoryId);

        if (!resData) {
            console.log('Load failed');
            return;
        }

        this.setState((preState) => {
            return {
                ...preState,
                products: resData,
            };
        });
    };

    private handlerRemoveProduct = async (productId?: string) => {
        if (!productId) {
            this.context.dispatchStore(
                this.context.actions.addMessageBox({
                    title: 'Lỗi',
                    icon: 'error',
                    message: 'ProductID === undefined',
                }),
            );
            return;
        }

        const resData = await api.product.remove(productId);
        if (!resData) {
            this.context.dispatchStore(
                this.context.actions.addMessageBox({
                    title: 'Xóa không thành công',
                    icon: 'error',
                    message: 'Có lỗi phía máy chủ! Hãy kiểm tra kết nối internet hoặc tải lại trang rồi thử lại',
                }),
            );
            return;
        }

        this.loadProducts();
    };

    private updateProductsEffect = make.effect((categoryId?: string) => {
        this.loadProducts(categoryId);
        this.setState((preState) => ({
            ...preState,
            categoryId,
        }));
    });

    public componentDidMount(): void {
        this.updateProductsEffect([this.props.params.categoryId]);
        super.componentDidMount();
    }

    public componentDidUpdate(
        prevProps: Readonly<TProductManagementPageProps>,
        prevState: Readonly<State>,
        snapshot?: any,
    ): void {
        this.updateProductsEffect([this.props.params.categoryId]);
    }

    public render() {
        return (
            <DefaultLayout>
                <Container>
                    <ProductManagementContext.Provider
                        value={{
                            loadProducts: this.loadProducts,
                            categoryId: this.state.categoryId,
                        }}
                    >
                        <ProductManagementMenuStrip />
                        <div className={styles['main']}>
                            <section className={styles['left']}>
                                <CategoryTree />
                            </section>
                            <section className={styles['right']}>
                                <ListProducts
                                    products={this.state.products}
                                    onRemoveClick={this.handlerRemoveProduct}
                                />
                            </section>
                        </div>
                    </ProductManagementContext.Provider>
                </Container>
            </DefaultLayout>
        );
    }
}
