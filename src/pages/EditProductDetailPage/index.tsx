import InsteadOfEditProductDetailPage, { IEditProductDetailPageProps } from './EditProductDetailPage';

import { useParams } from 'react-router-dom';

export default function EditProductDetailPage(props: Omit<IEditProductDetailPageProps, 'params'>) {
    return <InsteadOfEditProductDetailPage {...props} params={useParams()} />;
}
