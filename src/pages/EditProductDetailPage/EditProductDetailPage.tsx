import * as React from 'react';
import EditProductDetailArea from '../../components/pages/edit_product_detail/EditProductDetailArea';
import Container from '../../components/reuse/Container';
import EditProductDetailContainer from '../../containers/EditProductDetailContainer';
import EPages from '../../core/types/EPages';
import DefaultLayout from '../../layouts/DefaultLayout';
import storage from '../../storage';
import BasePage from '../BasePage';

export interface IEditProductDetailPageProps {
    params: any;
}

export interface IEditProductDetailPageState {}

export default class EditProductDetailPage extends BasePage<IEditProductDetailPageProps, IEditProductDetailPageState> {
    private id: string;

    public constructor(props: IEditProductDetailPageProps) {
        super(props, {});

        this.pageName = EPages.editProductDetail;
        this.id = props.params.id;
    }

    public render() {
        return (
            <DefaultLayout>
                <Container>
                    <storage.productDetail.Provider>
                        <EditProductDetailContainer id={this.id}>
                            <EditProductDetailArea />
                        </EditProductDetailContainer>
                    </storage.productDetail.Provider>
                </Container>
            </DefaultLayout>
        );
    }
}
